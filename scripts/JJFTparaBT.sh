#!/bin/bash



export OMP_NUM_THREADS=4
export MKL_THREADING_LAYER=sequential
prcs=1


M="20"
L=60
lboMd="100"
cut="1e-08"
lbocut="1e-08"



gam=$(awk -v x=2. 'BEGIN{print sqrt(x)}')
gams="1.414"
T="0.10"
omg="1.000"
omgp="0.000"
dt="0.01"
mpsN="MPSaT${T}FTdisptdvplboL${L}M${M}t01.000omg${omg}omgp${omgp}gam${gams}T0.100dt0.1000cut1e-09Md3000lboMd100lbocut1e-09.bin"
siteN="sitesetaT${T}FTdisptdvplboL${L}M${M}t01.000omg${omg}omgp${omgp}gam${gams}T0.100dt0.1000cut1e-09Md3000lboMd100lbocut1e-09.bin"


exe=JJFTpara_timeContBTsecimpl

tin=0.0000
tot=8.0000
dirname="JJFT/${exe}tot${tot}tin${tin}dt${dt}prcs${prcs}cut${cut}lbocut${lbocut}MPSaT${T}FTdisptdvplboL${L}M${M}t01.000omg${omg}omgp${omgp}gam${gams}T0.100dt0.1000cut1e-09Md3000lboMd100lbocut1e-09DIR"


if [ -d "$dirname" ]; then
  # Take action if $DIR exists. #
  echo "Installing config files in ${DIR}..."
  dirname="JJFT/${exe}tot${tot}tin${tin}dt${dt}prcs${prcs}cut${cut}lbocut${lbocut}MPSaT${T}FTdisptdvplboL${L}M${M}t01.000omg${omg}omgp${omgp}gam${gams}T0.100dt0.1000cut1e-09Md3000lboMd100lbocut1e-09DIR2"
fi
echo "hei"
mkdir ${dirname}
comm="mpirun -n ${prcs} ./bin/${exe} --lbocut ${lbocut} --cut ${cut} --mpsNK ${mpsN} --mpsNB ${mpsN} --dt ${dt} --siteN ${siteN}  --tot ${tot}  --L ${L} --gam ${gam} --M $M --omg ${omg} --omgp ${omgp} --appOP 1 --t_in ${tin} --d ${dirname}"

echo ${comm} >${dirname}/commando.txt
eval ${comm}




#mpirun -n 10 ./bin/${exe}
