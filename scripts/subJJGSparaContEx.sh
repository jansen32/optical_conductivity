#!/bin/bash
#SBATCH -p medium
#SBATCH -N 1
#SBATCH -n 15
#SBATCH --exclusive
#SBATCH --qos=long
#SBATCH -t 120:00:00
#SBATCH -o out.%J
#SBATCH -e err.%j
#SBATCH --cpus-per-task=2
#SBATCH --mail-type=ALL
#SBATCH --mail-user=david.jansen@uni-goettingen.de
#source /usr/users/jansen32/.bashrc
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/users/jansen32/usr/lib
export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK
export MKL_THREADING_LAYER=sequential
prcs=15
M="20"
L=60
lboMd="100" 
cut="1e-09"
lbocut="1e-09"
Md=3000

gam=$(awk -v x=2. 'BEGIN{print sqrt(x)}')
gams="1.414"


#echo ${gam}

omg="1.000"
omgp="-0.10"
dir="GSStates/"
start="9.0"
old_start="8.0"

dt=0.01
exe=JJpara_timeCont
tin=${start}
tot=10.
dirname_old="JJ/${exe}tot${start}tin${old_start}dt${dt}prcs${prcs}cut${cut}lbocut${lbocut}MPSGSdispdmrgL${L}M${M}MS40t01.000gam${gams}omega${omg}omegap${omgp}DIR/"
mpsNK="MPSKattot${start}MPSGSdispdmrgL${L}M${M}MS40t01.000gam${gams}omega${omg}omegap${omgp}.bin"
VAR1="${old_start}"
VAR2="0.0"

if [ "$VAR1" = "$VAR2" ]; then
    echo "Strings are equal."
    mpsNK="MPSKattot${start}appliedJMPSGSdispdmrgL${L}M${M}MS40t01.000gam${gams}omega${omg}omegap${omgp}.bin"
else
    echo "Strings are not equal."
fi

mpsNB="MPSBattot${start}MPSGSdispdmrgL${L}M${M}MS40t01.000gam${gams}omega${omg}omegap${omgp}.bin"
siteN="sitesetGSdispdmrgL${L}M${M}MS40t01.000gam${gams}omega${omg}omegap${omgp}.bin"
cp "${dirname_old}/${mpsNK}" $PWD/${mpsNK}
cp "${dirname_old}/${mpsNB}" $PWD/${mpsNB}
echo "${dirname_old}/${mpsNB}"

dirname="JJ/${exe}tot${tot}tin${tin}dt${dt}prcs${prcs}cut${cut}lbocut${lbocut}MPSGSdispdmrgL${L}M${M}MS40t01.000gam${gams}omega${omg}omegap${omgp}DIR"
if [ -d "$dirname" ]; then
  # Take action if $DIR exists. #
  echo "Installing config files in ${DIR}..."
  dirname="JJ/${exe}tot${tot}tin${tin}dt${dt}prcs${prcs}cut${cut}lbocut${lbocut}MPSGSdispdmrgL${L}M${M}MS40t01.000gam${gams}omega${omg}omegap${omgp}DIR2"
fi
mkdir ${dirname}
comm=" --n ${prcs} ./bin/${exe} --lbocut ${lbocut} --cut ${cut} --mpsNK ${mpsNK} --mpsNB ${mpsNB}  --siteN ${siteN}   --dt ${dt} --tot ${tot} --L ${L} --gam ${gam} --M $M --omg ${omg} --omgp ${omgp} --appOP 0 --t_in ${tin} --d ${dirname}" 
echo ${comm}
echo ${comm} >${dirname}/commando.txt
# #echo "Welcome $c times used 10"
 mpirun ${comm}
rm $PWD/${mpsNK}
rm $PWD/${mpsNB}







