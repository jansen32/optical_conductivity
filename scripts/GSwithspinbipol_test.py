#!/usr/bin/python
import sys, getopt
import numpy as np
import os
import subprocess
L=sys.argv[1]
M=sys.argv[2]
omgp=sys.argv[3]
gam=np.sqrt(2)
cut=1e-09
name="GSwithspinbipoldispdmrgL{0}M{1}MS200t01.000gam{2:.3f}omega1.000omegap{3}.bin".format(L,M,gam,omgp)
mpsn="MPS"+name
siten="siteset"+name
exe="GSwithspinbipol_test"
comm="time OMP_NUM_THREADS=4   ./bin/{0} --L {1} --M {2}  --gam {3}  --omgp {4} --mpsN {5} --siteN {6} --cut {7}".format(exe, L, M, gam, omgp, mpsn, siten, cut)


stdout=subprocess.check_call(comm, shell=True, stdout=sys.stdout, stderr=sys.stderr)
#
