#!/bin/bash
#SBATCH -p medium
#SBATCH -N 1
#SBATCH -n 16
#SBATCH --mem-per-cpu=3G
#SBATCH -t 20:00:00
#SBATCH -o out.%J
#SBATCH -e err.%j
#SBATCH --cpus-per-task=4
#SBATCH --mail-type=ALL
#SBATCH --mail-user=david.jansen@uni-goettingen.de
#SBATCH -C scratch
#source /usr/users/jansen32/.bashrc
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/users/jansen32/usr/lib
export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK
export MKL_THREADING_LAYER=sequential
prcs=$SLURM_NTASKS
prcs_old=16
M="30"
L=40
lboMd="300" 
cut="1e-08"
lbocut="1e-08"

md=50
gam=2.0
#$(awk -v x=2. 'BEGIN{print sqrt(x)}')
gams="2.000"


#echo ${gam}

omg="1.000"
omgp="0.000"
# alwaysuse string of size 3
old_start="6.0000"
start="12.000"
tot=18.000
T="1.00"
dt=0.01

exe=JJFTparawithspin_timeContsecimpl
tin=${start}

dirname_old="/scratch/users/$USER/JJFT/${exe}tot${start}tin${old_start}dt${dt}prcs${prcs_old}cut${cut}lbocut${lbocut}Md${md}MPSaT${T}FTwithspinbipoldisptdvplboL${L}M${M}t01.000omg${omg}omgp${omgp}gam${gams}T0.100dt0.1000cut1e-08Md3000lboMd300lbocut1e-08DIR/"
mpsNK="MPSKattot${start}MPSaT${T}FTwithspinbipoldisptdvplboL${L}M${M}t01.000omg${omg}omgp${omgp}gam${gams}T0.100dt0.1000cut1e-08Md3000lboMd300lbocut1e-08.bin"
mpsNB="MPSBattot${start}MPSaT${T}FTwithspinbipoldisptdvplboL${L}M${M}t01.000omg${omg}omgp${omgp}gam${gams}T0.100dt0.1000cut1e-08Md3000lboMd300lbocut1e-08.bin"
VAR1="${old_start}"
VAR2="0.0000"

if [ "$VAR1" = "$VAR2" ]; then
    echo "Strings are equal."
    mpsNK="MPSKattot${start}appliedJMPSaT${T}FTwithspinbipoldisptdvplboL${L}M${M}t01.000omg${omg}omgp${omgp}gam${gams}T0.100dt0.1000cut1e-08Md3000lboMd300lbocut1e-08.bin"
    mpsNB="MPSBattot${start}appliedJMPSaT${T}FTwithspinbipoldisptdvplboL${L}M${M}t01.000omg${omg}omgp${omgp}gam${gams}T0.100dt0.1000cut1e-08Md3000lboMd300lbocut1e-08.bin"
else
    echo "Strings are not equal."
fi


siteN="/scratch/users/$USER/sitesetaT${T}FTwithspinbipoldisptdvplboL${L}M${M}t01.000omg${omg}omgp${omgp}gam${gams}T0.100dt0.1000cut1e-08Md3000lboMd300lbocut1e-08.bin"
#cp "${dirname_old}/${mpsNK}" $PWD/${mpsNK}
#cp "${dirname_old}/${mpsNB}" $PWD/${mpsNB}
# echo "${dirname_old}/${mpsNB}"

dirname="/scratch/users/$USER/JJFT/${exe}tot${tot}tin${tin}dt${dt}prcs${prcs}cut${cut}lbocut${lbocut}Md${md}MPSaT${T}FTwithspinbipoldisptdvplboL${L}M${M}t01.000omg${omg}omgp${omgp}gam${gams}T0.100dt0.1000cut1e-08Md3000lboMd300lbocut1e-08DIR"
if [ -d "$dirname" ]; then
  # Take action if $DIR exists. #
  echo "Installing config files in ${DIR}..."
  dirname="/scratch/users/$USER/JJFT/${exe}tot${tot}tin${tin}dt${dt}prcs${prcs}cut${cut}lbocut${lbocut}Md${md}MPSaT${T}FTwithspinbipoldisptdvplboL${L}M${M}t01.000omg${omg}omgp${omgp}gam${gams}T0.100dt0.1000cut1e-08Md3000lboMd300lbocut1e-08DIR2"
fi
mkdir ${dirname}
comm="mpirun -n ${prcs} ./bin/${exe} --lbocut ${lbocut} --cut ${cut} --mpsNK ${dirname_old}${mpsNK} --mpsNB ${dirname_old}${mpsNB}  --siteN ${siteN}   --dt ${dt} --tot ${tot} --L ${L} --gam ${gam} --M $M --omg ${omg} --omgp ${omgp} --appOP 0 --t_in ${tin} --d ${dirname} --Md ${md}" 
echo $SLURM_JOB_ID >${dirname}/jobid.txt
echo ${comm} >${dirname}/commando.txt
# #echo "Welcome $c times used 10"
eval ${comm}
#rm $PWD/${mpsNK}
#rm $PWD/${mpsNB}







