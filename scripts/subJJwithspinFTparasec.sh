#!/bin/bash
#SBATCH -p medium
#SBATCH -N 1
#SBATCH -n 10
#SBATCH --mem-per-cpu=4G
#SBATCH -t 40:00:00
#SBATCH -o out.%J
#SBATCH -e err.%j
#SBATCH --cpus-per-task=4
#SBATCH --mail-type=ALL
#SBATCH --mail-user=david.jansen@uni-goettingen.de
#SBATCH -C scratch

#source /usr/users/jansen32/.bashrc
module load gcc/9.3.0                          
module load anaconda3/2021.05
module load intel-parallel-studio/cluster.2020.4


module load openmpi/4.1.1

export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/users/jansen32/usr/lib
export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK
export MKL_THREADING_LAYER=sequential
prcs=$SLURM_NTASKS

M="20"
L=20
lboMd="300"
cut="5e-08"
lbocut="1e-08"
md=2000

#export UCX_LOG_LEVEL=error
gam=1.0
#$(awk -v x=0.5 'BEGIN{print sqrt(x)}')
gams="1.000"
T="0.20"
omg="1.000"
omgp="0.000"
dt="0.01"
mpsN="/scratch/users/$USER/MPSaT${T}FTwithspinbipoldisptdvplboL${L}M${M}t01.000omg${omg}omgp${omgp}gam${gams}T0.100dt0.1000cut1e-09Md3000lboMd300lbocut1e-09.bin"
siteN="/scratch/users/$USER/sitesetaT${T}FTwithspinbipoldisptdvplboL${L}M${M}t01.000omg${omg}omgp${omgp}gam${gams}T0.100dt0.1000cut1e-09Md3000lboMd300lbocut1e-09.bin"


exe=JJFTparawithspin_timeContsecimpl

tin=0.0000
tot=5.0000
dirname="/scratch/users/$USER/JJFT/${exe}tot${tot}tin${tin}dt${dt}prcs${prcs}cut${cut}lbocut${lbocut}Md${md}MPSaT${T}FTwithspinbipoldisptdvplboL${L}M${M}t01.000omg${omg}omgp${omgp}gam${gams}T0.100dt0.1000cut1e-09Md3000lboMd300lbocut1e-09DIR"


if [ -d "$dirname" ]; then
  # Take action if $DIR exists. #
  echo "Installing config files in ${DIR}..."
  dirname="/scratch/users/$USER/JJFT/${exe}tot${tot}tin${tin}dt${dt}prcs${prcs}cut${cut}lbocut${lbocut}Md${md}MPSaT${T}FTwithspinbipoldisptdvplboL${L}M${M}t01.000omg${omg}omgp${omgp}gam${gams}T0.100dt0.1000cut1e-09Md3000lboMd300lbocut1e-09DIR2"
fi
echo "hei"
mkdir ${dirname}
comm="mpirun -n ${prcs} ./bin/${exe} --lbocut ${lbocut} --cut ${cut} --mpsNK ${mpsN} --mpsNB ${mpsN} --dt ${dt} --siteN ${siteN}  --tot ${tot}  --L ${L} --gam ${gam} --M $M --omg ${omg} --omgp ${omgp} --appOP 1 --t_in ${tin} --d ${dirname} --Md ${md} --lboMd ${lboMd}"

echo ${comm} >${dirname}/commando.txt
echo $SLURM_JOB_ID >${dirname}/jobid.txt
eval ${comm}




#mpirun -n 10 ./bin/${exe}
