##!/bin/bash

gam=1.0
#$(awk -v x=2. 'BEGIN{print sqrt(x)}')


cut=1e-09
lbocut=1e-09
L=5
M=3
t0=1.0
omg=1.0
dt=0.1
Md=3000
omgp=0.0
time OMP_NUM_THREADS=1 MKL_NUM_THREADS=3 ./bin/holFTW2 --L ${L} --omg ${omg} --t0 ${t0} --omgp ${omgp} --M ${M} --Md ${Md} --dt ${dt}  --gam ${gam}  --cut ${cut}   --T 0.1 --SA 1 |& tee FTW2gam${gam}L${L}M${M}t0${t0}omg${omg}cut${cut}omgp${omgp}dt${dt}.txt



