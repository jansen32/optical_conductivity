#!/usr/bin/python
import sys, getopt
import numpy as np
import os
import subprocess
L=sys.argv[1]
M=sys.argv[2]
omgp=sys.argv[3]
T=sys.argv[4]
gam=np.sqrt(2)
cut=1e-09
name="aT{0}FTwithspinbipoldisptdvplboL{1}M{2}t01.000omg1.000omgp{4}gam{3:.3f}T0.100dt0.1000cut1e-09Md3000lboMd300lbocut1e-09.bin".format(T,L,M,gam,omgp)
mpsn="MPS"+name
siten="siteset"+name
exe="FTwithspin_test"
comm="time OMP_NUM_THREADS=4   ./bin/{0} --L {1} --M {2}  --gam {3}  --omgp {4} --mpsN {5} --siteN {6} --cut {7}".format(exe, L, M, gam, omgp, mpsn, siten, cut)


stdout=subprocess.check_call(comm, shell=True, stdout=sys.stdout, stderr=sys.stderr)
#
