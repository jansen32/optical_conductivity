#!/bin/bash
#SBATCH -p medium
#SBATCH -N 1
#SBATCH -n 30
#SBATCH --mem-per-cpu=3G
#SBATCH -t 48:00:00
#SBATCH -o out.%J
#SBATCH -e err.%j
#SBATCH --cpus-per-task=2
#SBATCH --mail-type=ALL
#SBATCH --mail-user=david.jansen@uni-goettingen.de
#source /usr/users/jansen32/.bashrc
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/users/jansen32/usr/lib
export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK
export MKL_THREADING_LAYER=sequential
prcs=30
M="20"
L=60
lboMd="100"
cut="1e-08"
lbocut="1e-0"



gam=$(awk -v x=2. 'BEGIN{print sqrt(x)}')
gams="1.000"
tot=8.0 # make always str of size 3
T="0.40"
omg="1.000"
omgp="0.000"
dt="0.01"
mpsN="MPSaT${T}FTdisptdvplboL${L}M${M}t01.000omg${omg}omgp${omgp}gam${gams}T0.100dt0.1000cut1e-09Md3000lboMd100lbocut1e-09.bin"
siteN="sitesetaT${T}FTdisptdvplboL${L}M${M}t01.000omg${omg}omgp${omgp}gam${gams}T0.100dt0.1000cut1e-09Md3000lboMd100lbocut1e-09.bin"


exe=JJFTpara_timeCont

tin=0.0
dirname="JJFT/${exe}tot${tot}tin${tin}dt${dt}prcs${prcs}cut${cut}lbocut${lbocut}MPSaT${T}FTdisptdvplboL${L}M${M}t01.000omg${omg}omgp${omgp}gam${gams}T0.100dt0.1000cut1e-09Md3000lboMd100lbocut1e-09DIR"
if [ -d "$dirname" ]; then
  # Take action if $DIR exists. #
  echo "Installing config files in ${DIR}..."
  dirname="JJFT/${exe}tot${tot}tin${tin}dt${dt}prcs${prcs}cut${cut}lbocut${lbocut}MPSaT${T}FTdisptdvplboL${L}M${M}t01.000omg${omg}omgp${omgp}gam${gams}T0.100dt0.1000cut1e-09Md3000lboMd100lbocut1e-09DIR2"
fi
mkdir ${dirname}
comm=" --n ${prcs} ./bin/${exe} --lbocut ${lbocut} --cut ${cut} --mpsNK ${mpsN} --mpsNB ${mpsN} --dt ${dt} --siteN ${siteN}  --tot ${tot}  --L ${L} --gam ${gam} --M $M --omg ${omg} --omgp ${omgp} --appOP 1 --t_in ${tin} --d ${dirname}"

echo ${comm}
echo ${comm} >${dirname}/commando.txt
mpirun ${comm}

