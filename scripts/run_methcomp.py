#!/usr/bin/python
import sys
import numpy as np
import os
import subprocess
import shlex

M="25"
L=13



gam=2.0
#np.sqrt(2)
omg=1.0
omgp=0.1
t0=1.0
tot=15.
dt=0.01
cut=1e-09
lbocut=1e-08
Md=80
binary="holtdvp_lbo_methcomp"
dirname="{0}L{1}M{2}t0{3:.3f}gam{4:.3f}omg{5:.3f}omgp{10:.3f}tot{6:.3f}dt{7:.3f}cut{8}lbocut{9}Md{11}DIR".format(binary,L,M,t0,gam, omg,tot,dt,cut,lbocut ,omgp, Md)
os.mkdir(dirname)
comm="time MKL_THREADING_LAYER=sequential OMP_NUM_THREADS=2 ./bin/{0} --L {1} --M {2} --t0 {3} --gam {4} --omg {5} --tot {6} --dt {7} --cut {8} --lbocut {9} --d {10} --omgp {11} --Md {12} |& tee {10}/log.txt ".format(binary,L,M,t0,gam, omg,tot,dt,cut,lbocut, dirname , omgp, Md)
print(comm)

with open(dirname+'/comm.txt', 'w') as f:
    f.write(comm)
comm_list=shlex.split(comm)

stdout=subprocess.check_call(comm, shell=True, stdout=sys.stdout)

