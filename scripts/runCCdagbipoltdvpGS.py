
#!/usr/bin/python
import sys
import numpy as np
import os
import subprocess
import shlex

exe="CCdagbipoltdvplbonoNormGS"

L=8
M=20

t0=1.0
gam=1.0
omg=1.0
omgp=0.
dt=0.01
tot=20
cut=1e-08
lbocut=1e-08
Md=3000
i=int((L+1)/2)
j=8
nam="GSwithspinbipoldispdmrgL{0}M{1}MS200t01.000gam{2:.3f}omega1.000omegap{3:.3f}.bin".format(L,M,gam,omgp)
mpsN="MPS"+nam
siteN="siteset"+nam



dirname="{0}i{1}j{2}L{3}M{4}t0{5:.3f}gam{6:.3f}omg{7:.3f}omgp{8:.3f}dt{9:.5f}tot{10:.3f}cut{11}lbocut{12}Md{13}DIR".format(exe,i,j, L,M,t0,gam,omg,omgp,dt,tot,cut,lbocut,Md)
    
if(os.path.isdir(dirname)):
    dirname+="2"
if(os.path.isdir(dirname)):
    dirname+="2"
if(os.path.isdir(dirname)):
    print("to many versions of dirName exist")
    sys.exit()
os.mkdir(dirname)
comm="time MKL_THREADING_LAYER=sequential OMP_NUM_THREADS=2 ./bin/{0} --d {1} --L {2} --M {3}  --t0 {4} --gam {5} --omg {6} --omgp {7} --dt {8} --tot {9} --cut {10} --lbocut {11} --Md {12}  --mpsN {13} --siteN {14} --i {15} --j {16} | tee {1}/log.txt".format(exe,dirname, L,M,t0,gam,omg,omgp,dt,tot,cut,lbocut,Md, mpsN, siteN, i ,j)
print(comm)
comm_file=open(dirname+"/command.txt","w")
comm_file.write(comm)
comm_file.close()
stdout=subprocess.check_call(comm, shell=True, stdout=sys.stdout, stderr=sys.stderr)




# for j in np.arange(1,L+1,1):


#     dirname="{0}i{1}j{2}L{3}M{4}t0{5:.3f}gam{6:.3f}omg{7:.3f}omgp{8:.3f}dt{9:.5f}tot{10:.3f}cut{11}lbocut{12}Md{13}DIR".format(exe,i,j, L,M,t0,gam,omg,omgp,dt,tot,cut,lbocut,Md)
    
#     if(os.path.isdir(dirname)):
#         dirname+="2"
#     if(os.path.isdir(dirname)):
#         dirname+="2"
#     if(os.path.isdir(dirname)):
#         print("to many versions of dirName exist")
#         sys.exit()
#     os.mkdir(dirname)
#     comm="time MKL_THREADING_LAYER=sequential OMP_NUM_THREADS=2 ./bin/{0} --d {1} --L {2} --M {3}  --t0 {4} --gam {5} --omg {6} --omgp {7} --dt {8} --tot {9} --cut {10} --lbocut {11} --Md {12}  --mpsN {13} --siteN {14} --i {15} --j {16} | tee {1}/log.txt".format(exe,dirname, L,M,t0,gam,omg,omgp,dt,tot,cut,lbocut,Md, mpsN, siteN, i ,j)
#     print(comm)
#     comm_file=open(dirname+"/command.txt","w")
#     comm_file.write(comm)
#     comm_file.close()
#     stdout=subprocess.check_call(comm, shell=True, stdout=sys.stdout, stderr=sys.stderr)


