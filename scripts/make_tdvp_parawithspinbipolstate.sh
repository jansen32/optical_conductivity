#!/bin/bash



export OMP_NUM_THREADS=2
export MKL_THREADING_LAYER=sequential
prcs=2




cut="1e-08"
lbocut="1e-08"
gam=$(awk -v x=2. 'BEGIN{print sqrt(x)}')
L=3
M=5
t0=1.0
omg=1.0
dt=0.1
Md=3000
omgp=0.
exe=holFTparaholtwospin_state
mpirun -n ${prcs} ./bin/${exe}  --L ${L} --omg ${omg} --t0 ${t0} --omgp ${omgp} --M ${M} --Md ${Md} --dt ${dt}  --gam ${gam}  --cut ${cut}  --lbocut ${lbocut}  --T 0.1 --SA 1 |& tee FTparaprcs${prcs}withspinbipoltdvptsgam${gam}L${L}M${M}t0${t0}omg${omg}cut${cut}lbocut${lbocut}omgp${omgp}dt${dt}.txt



