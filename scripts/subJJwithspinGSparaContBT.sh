#!/bin/bash
#SBATCH -p medium
#SBATCH -N 1
#SBATCH -n 8
#SBATCH --mem-per-cpu=4G
#SBATCH -t 40:00:00
#SBATCH -o out.%J
#SBATCH -e err.%j
#SBATCH --cpus-per-task=4
#SBATCH --mail-type=ALL
#SBATCH --mail-user=david.jansen@uni-goettingen.de
#SBATCH -C scratch
#source /usr/users/jansen32/.bashrc

export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/users/jansen32/usr/lib
export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK
export MKL_THREADING_LAYER=sequential
prcs=$SLURM_NTASKS
prcs_old=8
M="20"
L=40
lboMd="400" 
cut="1e-08"
lbocut="1e-08"
Md=3000

gam=$(awk -v x=2. 'BEGIN{print sqrt(x)}')
#$(awk -v x=1.6 'BEGIN{print sqrt(x)}')
#
gams="1.414"


#echo ${gam}

omg="1.000"
omgp="0.100"

old_start="7.0000"
start="8.0000"


dt=0.01
exe=JJparawithspin_timeContBTsecimpl

tin=${start}
tot=9.0000


dirname_old="/scratch/users/$USER/JJ/${exe}tot${start}tin${old_start}dt${dt}prcs${prcs_old}cut${cut}lbocut${lbocut}MPSGSwithspinbipoldispdmrgL${L}M${M}MS200t01.000gam${gams}omega${omg}omegap${omgp}DIR/"
mpsNK="MPSKattot${start}MPSGSwithspinbipoldispdmrgL${L}M${M}MS200t01.000gam${gams}omega${omg}omegap${omgp}.bin"
VAR1="${old_start}"
VAR2="0.0000"

if [ "$VAR1" = "$VAR2" ]; then
    echo "Strings are equal."
    mpsNK="MPSKattot${start}appliedJMPSGSwithspinbipoldispdmrgL${L}M${M}MS200t01.000gam${gams}omega${omg}omegap${omgp}.bin"
else
    echo "Strings are not equal."
fi

mpsNB="MPSBattot${start}MPSGSwithspinbipoldispdmrgL${L}M${M}MS200t01.000gam${gams}omega${omg}omegap${omgp}.bin"
siteN="/scratch/users/$USER/sitesetGSwithspinbipoldispdmrgL${L}M${M}MS200t01.000gam${gams}omega${omg}omegap${omgp}.bin"


dirname="/scratch/users/$USER/JJ/${exe}tot${tot}tin${tin}dt${dt}prcs${prcs}cut${cut}lbocut${lbocut}MPSGSwithspinbipoldispdmrgL${L}M${M}MS200t01.000gam${gams}omega${omg}omegap${omgp}DIR"
if [ -d "$dirname" ]; then
  # Take action if $DIR exists. #
  echo "Installing config files in ${DIR}..."
  dirname="/scratch/users/$USER/JJ/${exe}tot${tot}tin${tin}dt${dt}prcs${prcs}cut${cut}lbocut${lbocut}MPSGSwithspinbipoldispdmrgL${L}M${M}MS200t01.000gam${gams}omega${omg}omegap${omgp}DIR2"
fi
mkdir ${dirname}
comm="mpirun -n ${prcs} ./bin/${exe} --lbocut ${lbocut} --cut ${cut} --mpsNK ${dirname_old}${mpsNK} --mpsNB ${dirname_old}${mpsNB}  --siteN ${siteN}   --dt ${dt} --tot ${tot} --L ${L} --gam ${gam} --M $M --omg ${omg} --omgp ${omgp} --appOP 0 --t_in ${tin} --d ${dirname}" 

echo ${comm} >${dirname}/commando.txt
echo $SLURM_JOB_ID >${dirname}/jobid.txt
eval ${comm}
# #echo "Welcome $c times used 10"

#rm $PWD/${mpsNK}
#rm $PWD/${mpsNB}







