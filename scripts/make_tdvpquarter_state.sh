
##!/bin/bash

gam=$(awk -v x=2.0 'BEGIN{print sqrt(x)}')
#$(awk -v x=1.6 'BEGIN{print sqrt(x)}')


cut=1e-08
lbocut=1e-08
L=24
M=20
t0=1.0
omg=1.0
dt=0.1
Md=3000
omgp=0.0
time MKL_THREADING_LAYER=sequential OMP_NUM_THREADS=3 ./bin/holFTquarterfilledtdvp_lbo_ts --L ${L} --omg ${omg} --t0 ${t0} --omgp ${omgp} --M ${M} --Md ${Md} --dt ${dt}  --gam ${gam}  --cut ${cut}  --lbocut ${lbocut}  --T 0.1 --SA 1 |& tee FTtdvptsquarterfillgam${gam}L${L}M${M}t0${t0}omg${omg}cut${cut}lbocut${lbocut}omgp${omgp}dt${dt}.txt



