#!/bin/bash
#SBATCH -p medium
#SBATCH -N 1
#SBATCH -n 12
#SBATCH --mem-per-cpu=4G
#SBATCH -t 48:00:00
#SBATCH -o out.%J
#SBATCH -e err.%j
#SBATCH --cpus-per-task=2
#SBATCH --mail-type=ALL
#SBATCH --mail-user=david.jansen@uni-goettingen.de
#source /usr/users/jansen32/.bashrc
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/users/jansen32/usr/lib
export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK
export MKL_THREADING_LAYER=sequential
prcs=$SLURM_NTASKS
M="20"
L=60
lboMd="100" 
cut="1e-09"
lbocut="1e-08"
Md=3000

gam=$(awk -v x=2. 'BEGIN{print sqrt(x)}')
gams="1.414"
tot=5.0

#echo ${gam}

omg="1.000"
omgp="-0.10"
dir="GSStates/"
mpsN="MPSGSdispdmrgL${L}M${M}MS200t01.000gam${gams}omega${omg}omegap${omgp}.bin"
siteN="sitesetGSdispdmrgL${L}M${M}MS200t01.000gam${gams}omega${omg}omegap${omgp}.bin"
dt=0.01
exe=JJpara_timeCont
tin=0.0
dirname="JJ/${exe}tot${tot}tin${tin}dt${dt}prcs${prcs}cut${cut}lbocut${lbocut}MPSGSdispdmrgL${L}M${M}MS200t01.000gam${gams}omega${omg}omegap${omgp}DIR"
if [ -d "$dirname" ]; then
  # Take action if $DIR exists. #
  echo "Installing config files in ${DIR}..."
  dirname="JJ/${exe}tot${tot}tin${tin}dt${dt}prcs${prcs}cut${cut}lbocut${lbocut}MPSGSdispdmrgL${L}M${M}MS200t01.000gam${gams}omega${omg}omegap${omgp}DIR2"
fi
mkdir ${dirname}
comm=" mpirun -n ${prcs} ./bin/${exe} --lbocut ${lbocut} --cut ${cut} --mpsNK ${mpsN} --mpsNB ${mpsN}  --siteN ${siteN}   --dt ${dt} --tot ${tot} --L ${L} --gam ${gam} --M $M --omg ${omg} --omgp ${omgp} --appOP 1 --t_in ${tin} --d ${dirname}" 
echo ${comm} >${dirname}/commando.txt
echo $SLURM_JOB_ID >${dirname}/jobid.txt

eval ${comm}





