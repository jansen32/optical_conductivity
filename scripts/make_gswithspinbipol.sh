##!/bin/bash

gam=2.0
#$(awk -v x=2.0 'BEGIN{print sqrt(x)}')



L=20
M=30
t0=1.0
omg=1.0

omgp=-0.1
MS=200
time MKL_THREADING_LAYER=sequential OMP_NUM_THREADS=3 ./bin/holGSwithspinbipoldis --L ${L} --omg ${omg} --t0 ${t0} --omgp ${omgp} --M ${M}   --gam ${gam}  --MS ${MS}   --SS 1 |& tee gswithspinbipoldispgam${gam}L${L}M${M}t0${t0}omg${omg}omgp${omgp}MS${MS}.txt









