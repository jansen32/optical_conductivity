##!/bin/bash

gam=1.0
#$(awk -v x=2. 'BEGIN{print sqrt(x)}')


cut=1e-09
lbocut=1e-09
L=60
M=20
t0=1.0
omg=1.0
dt=0.1
Md=3000
omgp=-0.1
time MKL_THREADING_LAYER=sequential OMP_NUM_THREADS=3 ./bin/holFTtdmrglboFast --L ${L} --omg ${omg} --t0 ${t0} --omgp ${omgp} --M ${M} --Md ${Md} --dt ${dt}  --gam ${gam}  --cut ${cut} --lbocut ${lbocut}  --T 0.1 --SA 1 |& tee FTtdmrggam${gam}L${L}M${M}t0${t0}omg${omg}omgp${omgp}cut${cut}lbocut${lbocut}dt${dt}.txt


