#!/bin/bash
#SBATCH -p medium
#SBATCH -N 1
#SBATCH -n 12
#SBATCH --mem-per-cpu=4G
#SBATCH -t 18:00:00
#SBATCH -o out.%J
#SBATCH -e err.%j
#SBATCH --cpus-per-task=2
#SBATCH --mail-type=ALL
#SBATCH --mail-user=david.jansen@uni-goettingen.de


#source /usr/users/jansen32/.bashrc


export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/users/jansen32/usr/lib
export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK
export MKL_THREADING_LAYER=sequential
prcs=$SLURM_NTASKS
M="30"
L=30
lboMd="100"
cut="1e-10"
lbocut="1e-08"


#export UCX_LOG_LEVEL=error
gam=3.0
#$(awk -v x=2. 'BEGIN{print sqrt(x)}')
gams="3.000"
tot=5.0000 # make always str of size 6
T="0.10"
omg="1.000"
omgp="0.000"
dt="0.01"
mpsN="MPSaT${T}FTdisptdvplboL${L}M${M}t01.000omg${omg}omgp${omgp}gam${gams}T0.100dt0.1000cut1e-09Md3000lboMd100lbocut1e-09.bin"
siteN="sitesetaT${T}FTdisptdvplboL${L}M${M}t01.000omg${omg}omgp${omgp}gam${gams}T0.100dt0.1000cut1e-09Md3000lboMd100lbocut1e-09.bin"


exe=JJFTpara_timeContBT

tin=0.0000
dirname="JJFT/${exe}tot${tot}tin${tin}dt${dt}prcs${prcs}cut${cut}lbocut${lbocut}MPSaT${T}FTdisptdvplboL${L}M${M}t01.000omg${omg}omgp${omgp}gam${gams}T0.100dt0.1000cut1e-09Md3000lboMd100lbocut1e-09DIR"
pwd
ls
echo "hei"
if [ -d "$dirname" ]; then
  # Take action if $DIR exists. #
  echo "Installing config files in ${DIR}..."
  dirname="JJFT/${exe}tot${tot}tin${tin}dt${dt}prcs${prcs}cut${cut}lbocut${lbocut}MPSaT${T}FTdisptdvplboL${L}M${M}t01.000omg${omg}omgp${omgp}gam${gams}T0.100dt0.1000cut1e-09Md3000lboMd100lbocut1e-09DIR2"
fi
echo "hei"
mkdir ${dirname}
comm="mpirun -n ${prcs} ./bin/${exe} --lbocut ${lbocut} --cut ${cut} --mpsNK ${mpsN} --mpsNB ${mpsN} --dt ${dt} --siteN ${siteN}  --tot ${tot}  --L ${L} --gam ${gam} --M $M --omg ${omg} --omgp ${omgp} --appOP 1 --t_in ${tin} --d ${dirname}"

echo ${comm} >${dirname}/commando.txt
echo $SLURM_JOB_ID >${dirname}/jobid.txt
eval ${comm}




#mpirun -n 10 ./bin/${exe}
