#!/bin/bash
#SBATCH -p medium
#SBATCH -N 1
#SBATCH -n 2
#SBATCH --mem-per-cpu=1G
#SBATCH -t 2:00:00
#SBATCH -o out.%J
#SBATCH -e err.%j
#SBATCH --cpus-per-task=2
#SBATCH --mail-type=ALL
#SBATCH --mail-user=david.jansen@uni-goettingen.de
#SBATCH -C scratch
#source /usr/users/jansen32/.bashrc
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/users/jansen32/usr/lib
export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK
export MKL_THREADING_LAYER=sequential
prcs=$SLURM_NTASKS
prcs_old=24
M="20"
L=60
lboMd="300" 
cut="1e-08"
lbocut="1e-08"
md=3000

gam=1.0
#$(awk -v x=2. 'BEGIN{print sqrt(x)}')
gams="1.000"


#echo ${gam}

omg="1.000"
omgp="0.100"
# alwaysuse string of size 3
old_start="0.0000"
start="10.000"
tot=11.000
T="0.10"
dt=0.01

exe=JJFTpara_Testing

tin=${start}
dirname_old="/scratch/users/$USER/JJFT/JJFTpara_timeContsecimpltot${start}tin${old_start}dt${dt}prcs${prcs_old}cut${cut}lbocut${lbocut}Md${md}MPSaT${T}FTdisptdvplboL${L}M${M}t01.000omg${omg}omgp${omgp}gam${gams}T0.100dt0.1000cut1e-09Md3000lboMd100lbocut1e-09DIR/"
mpsNK="MPSKattot${start}MPSaT${T}FTdisptdvplboL${L}M${M}t01.000omg${omg}omgp${omgp}gam${gams}T0.100dt0.1000cut1e-09Md3000lboMd100lbocut1e-09.bin"
mpsNB="MPSBattot${start}MPSaT${T}FTdisptdvplboL${L}M${M}t01.000omg${omg}omgp${omgp}gam${gams}T0.100dt0.1000cut1e-09Md3000lboMd100lbocut1e-09.bin"
VAR1="${old_start}"
VAR2="0.0000"

if [ "$VAR1" = "$VAR2" ]; then
    echo "Strings are equal."
    mpsNK="MPSKattot${start}appliedJMPSaT${T}FTdisptdvplboL${L}M${M}t01.000omg${omg}omgp${omgp}gam${gams}T0.100dt0.1000cut1e-09Md3000lboMd100lbocut1e-09.bin"
    mpsNB="MPSBattot${start}appliedJMPSaT${T}FTdisptdvplboL${L}M${M}t01.000omg${omg}omgp${omgp}gam${gams}T0.100dt0.1000cut1e-09Md3000lboMd100lbocut1e-09.bin"
else
    echo "Strings are not equal."
fi


siteN="/scratch/users/$USER/sitesetaT${T}FTdisptdvplboL${L}M${M}t01.000omg${omg}omgp${omgp}gam${gams}T0.100dt0.1000cut1e-09Md3000lboMd100lbocut1e-09.bin"
#cp "${dirname_old}/${mpsNK}" $PWD/${mpsNK}
#cp "${dirname_old}/${mpsNB}" $PWD/${mpsNB}
# echo "${dirname_old}/${mpsNB}"

dirname="/scratch/users/$USER/JJFT/JJFTpara_timeContsecimpltot${tot}tin${tin}dt${dt}prcs${prcs}cut${cut}lbocut${lbocut}Md${md}MPSaT${T}FTdisptdvplboL${L}M${M}t01.000omg${omg}omgp${omgp}gam${gams}T0.100dt0.1000cut1e-09Md3000lboMd100lbocut1e-09DIR"
if [ -d "$dirname" ]; then
  # Take action if $DIR exists. #
  echo "Installing config files in ${DIR}..."
  dirname="/scratch/users/$USER/JJFT/JJFTpara_timeContsecimpltot${tot}tin${tin}dt${dt}prcs${prcs}cut${cut}lbocut${lbocut}Md${md}MPSaT${T}FTdisptdvplboL${L}M${M}t01.000omg${omg}omgp${omgp}gam${gams}T0.100dt0.1000cut1e-09Md3000lboMd100lbocut1e-09DIR2"
fi
#mkdir ${dirname}
comm="mpirun -n ${prcs} ./bin/${exe} --lbocut ${lbocut} --cut ${cut} --mpsNK ${dirname_old}${mpsNK} --mpsNB ${dirname_old}${mpsNB}  --siteN ${siteN}   --dt ${dt} --tot ${tot} --L ${L} --gam ${gam} --M $M --omg ${omg} --omgp ${omgp} --appOP 0 --t_in ${tin} --d ${dirname}" 
echo $SLURM_JOB_ID >${dirname}/jobid.txt
echo ${comm} >${dirname}/commando.txt
# #echo "Welcome $c times used 10"
eval ${comm}
#rm $PWD/${mpsNK}
#rm $PWD/${mpsNB}







