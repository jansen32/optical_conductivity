#!/usr/bin/python
import sys
import numpy as np
import os
import subprocess
gam=1.00
L=60
M=20
omgp=0.100
t0=1.0
omg=1.0
name="GSdispdmrgL{0}M{1}MS200t01.000gam{2:.3f}omega1.000omegap{3:.3f}.bin".format(L,M,gam, omgp)
mpsN="MPS"+name
siteN="siteset"+name

dt=0.01
tot="8.0"
cut="1e-08"
lbocut="1e-08"

exe="JJtdmrglbonoNormGS"
dirName="JJtdmrg/{0}tot{1}dt{2:.3f}cut{3}lbocut{4}{5}DIR".format(exe,tot, dt, cut, lbocut, mpsN[:-4])



print(dirName)
if(os.path.isdir(dirName)):
     dirName+="2"
     if(os.path.isdir(dirName)):
         dirName+="2"
         if(os.path.isdir(dirName)):
             print("to many versions of dirName exist")
             sys.exit()
os.mkdir(dirName)

comm="MKL_THREADING_LAYER=sequential OMP_NUM_THREADS=4 ./bin/{0} --L {1} --M {2} --t0 {3} --gam {4} --omg {5} --omgp {6} --dt {7} --tot {8} --d {9}/ --mpsN {10} --siteN {11} --cut {12} --lbocut {13}".format(exe, L, M, t0, gam, omg, omgp, dt, tot, dirName, mpsN, siteN, cut, lbocut)

comm_file=open(dirName+"/command.txt","w")
comm_file.write(comm)
comm_file.close()
stdout=subprocess.check_call(comm, shell=True, stdout=sys.stdout, stderr=sys.stderr)
#
