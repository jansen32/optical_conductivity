#!/bin/bash
#!/bin/bash
#SBATCH -p medium
#SBATCH -N 1
#SBATCH -n 30
#SBATCH --exclusive
#SBATCH -t 48:00:00
#SBATCH -o out.%J
#SBATCH -e err.%j
#SBATCH --cpus-per-task=2
#SBATCH --mail-type=ALL
#SBATCH --mail-user=david.jansen@uni-goettingen.de
#source /usr/users/jansen32/.bashrc
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/users/jansen32/usr/lib
export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK
export MKL_THREADING_LAYER=sequential
prcs=30
prcs_old=30
M="20"
L=60
lboMd="100" 
cut="1e-09"
lbocut="1e-09"


gam=$(awk -v x=2. 'BEGIN{print sqrt(x)}')
gams="1.414"


#echo ${gam}

omg="1.000"
omgp="-0.10"
# alwaysuse string of size 3
old_start="8.0"
start="9.0"
tot=9.5
T="0.20"
dt=0.01
exe=JJFTpara_timeCont
tin=${start}

dirname_old="JJFT/${exe}tot${start}tin${old_start}dt${dt}prcs${prcs_old}cut${cut}lbocut${lbocut}MPSaT${T}FTdisptdvplboL${L}M${M}t01.000omg${omg}omgp${omgp}gam${gams}T0.100dt0.1000cut1e-09Md3000lboMd100lbocut1e-09DIR"
mpsNK="MPSKattot${start}MPSaT${T}FTdisptdvplboL${L}M${M}t01.000omg${omg}omgp${omgp}gam${gams}T0.100dt0.1000cut1e-09Md3000lboMd100lbocut1e-09.bin"
mpsNB="MPSBattot${start}MPSaT${T}FTdisptdvplboL${L}M${M}t01.000omg${omg}omgp${omgp}gam${gams}T0.100dt0.1000cut1e-09Md3000lboMd100lbocut1e-09.bin"
VAR1="${old_start}"
VAR2="0.0"

if [ "$VAR1" = "$VAR2" ]; then
    echo "Strings are equal."
    mpsNK="MPSKattot${start}appliedJMPSaT${T}FTdisptdvplboL${L}M${M}t01.000omg${omg}omgp${omgp}gam${gams}T0.100dt0.1000cut1e-09Md3000lboMd100lbocut1e-09.bin"
    mpsNB="MPSBattot${start}appliedJMPSaT${T}FTdisptdvplboL${L}M${M}t01.000omg${omg}omgp${omgp}gam${gams}T0.100dt0.1000cut1e-09Md3000lboMd100lbocut1e-09.bin"
else
    echo "Strings are not equal."
fi


siteN="sitesetaT${T}FTdisptdvplboL${L}M${M}t01.000omg${omg}omgp${omgp}gam${gams}T0.100dt0.1000cut1e-09Md3000lboMd100lbocut1e-09.bin"
#cp "${dirname_old}/${mpsNK}" $PWD/${mpsNK}
#cp "${dirname_old}/${mpsNB}" $PWD/${mpsNB}
# echo "${dirname_old}/${mpsNB}"

dirname="JJFT/${exe}tot${tot}tin${tin}dt${dt}prcs${prcs}cut${cut}lbocut${lbocut}MPSaT${T}FTdisptdvplboL${L}M${M}t01.000omg${omg}omgp${omgp}gam${gams}T0.100dt0.1000cut1e-09Md3000lboMd100lbocut1e-09DIR"
if [ -d "$dirname" ]; then
  # Take action if $DIR exists. #
  echo "Installing config files in ${DIR}..."
  dirname="JJFT/${exe}tot${tot}tin${tin}dt${dt}prcs${prcs}cut${cut}lbocut${lbocut}MPSaT${T}FTdisptdvplboL${L}M${M}t01.000omg${omg}omgp${omgp}gam${gams}T0.100dt0.1000cut1e-09Md3000lboMd100lbocut1e-09DIR2"
fi
mkdir ${dirname}
comm=" --n ${prcs} ./bin/${exe} --lbocut ${lbocut} --cut ${cut} --mpsNK ${mpsNK} --mpsNB ${mpsNB}  --siteN ${siteN}   --dt ${dt} --tot ${tot} --L ${L} --gam ${gam} --M $M --omg ${omg} --omgp ${omgp} --appOP 0 --t_in ${tin} --d ${dirname}" 
echo ${comm}
echo ${comm} >${dirname}/commando.txt
# #echo "Welcome $c times used 10"
 mpirun ${comm}
#rm $PWD/${mpsNK}
#rm $PWD/${mpsNB}







