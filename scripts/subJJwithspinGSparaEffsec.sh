#!/bin/bash
#SBATCH -p medium
#SBATCH -N 1
#SBATCH -n 4
#SBATCH --mem-per-cpu=4G
#SBATCH -t 40:00:00
#SBATCH -o out.%J
#SBATCH -e err.%j
#SBATCH --cpus-per-task=4
#SBATCH --mail-type=ALL
#SBATCH --mail-user=david.jansen@uni-goettingen.de
#source /usr/users/jansen32/.bashrc
#SBATCH -C scratch
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/users/jansen32/usr/lib
export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK
export MKL_THREADING_LAYER=sequential
prcs=$SLURM_NTASKS

M="35"
L=20
lboMd="400"
cut="1e-06"
lbocut="1e-07"
md=2000

gam=2.0
#$(awk -v x=0.5 'BEGIN{print sqrt(x)}')
#$(awk -v x=2. 'BEGIN{print sqrt(x)}')
gams="2.000"
tot=8.0000

#echo ${gam}

omg="1.000"
omgp="0.100"

mpsN="/scratch/users/$USER/MPSGSwithspinbipoldispdmrgL${L}M${M}MS200t01.000gam${gams}omega${omg}omegap${omgp}.bin"
siteN="/scratch/users/$USER/sitesetGSwithspinbipoldispdmrgL${L}M${M}MS200t01.000gam${gams}omega${omg}omegap${omgp}.bin"
dt=0.01
exe=JJparawithspin_timeContsecimplEff
tin=0.0000
dirname="/scratch/users/$USER/JJ/${exe}tot${tot}tin${tin}dt${dt}prcs${prcs}cut${cut}lbocut${lbocut}Md${md}MPSGSwithspinbipoldispdmrgL${L}M${M}MS200t01.000gam${gams}omega${omg}omegap${omgp}DIR"
if [ -d "$dirname" ]; then
  # Take action if $DIR exists. #
  echo "Installing config files in ${DIR}..."
  dirname="/scratch/users/$USER/JJ/${exe}tot${tot}tin${tin}dt${dt}prcs${prcs}cut${cut}lbocut${lbocut}Md${md}MPSGSwithspinbipoldispdmrgL${L}M${M}MS200t01.000gam${gams}omega${omg}omegap${omgp}DIR2"
fi
mkdir ${dirname}
comm=" mpirun -n ${prcs} ./bin/${exe} --lbocut ${lbocut} --cut ${cut} --mpsNK ${mpsN} --mpsNB ${mpsN}  --siteN ${siteN}   --dt ${dt} --tot ${tot} --L ${L} --gam ${gam} --M $M --omg ${omg} --omgp ${omgp} --appOP 1 --t_in ${tin} --d ${dirname} --Md ${md}"
echo ${comm} >${dirname}/commando.txt
echo $SLURM_JOB_ID >${dirname}/jobid.txt
eval ${comm}
