Here is the codee used for the computations for [Finite-temperature optical conductivity with density-matrix renormalization group methods for the Holstein polaron and bipolaron with dispersive phonons](https://arxiv.org/abs/2206.00985).
The source files are in the "src" directory and the scripts to run them are in "scripts" directory. The Makefile will need to be adapted depending on the working directory.

## The important files in the scripts directory are
<ul>
 <li> scripts/make_tdvp_state.sh : Make polaron finite temperature state </li>
 <li> scripts/make_tdvp_withspinbipolstate.sh : Make bipolaron finite temperature state </li>
 <li> scripts/runJJtdvpOneSite.py : Run single site TDVP for the polaron </li>
 <li> scripts/run_methcomp.py :  Compare different TDVP implementations (Fig. 17) </li>
 <li> scripts/subJJFTparasec.sh : Starting p2TDVP for the polaron at finite temperature on the GWDG cluster </li>
 <li> scripts/subJJFTparasecCont.sh : Continuing p2TDVP for the polaron at finite temperature on the GWDG cluster </li>
 <li> scripts/subJJwithspinFTparasec.sh : Starting p2TDVP for the bipolaron at finite temperature on the GWDG cluster </li>
 <li> scripts/subJJwithspinFTparaContsec.sh :  Continuing p2TDVP for the bipolaron at finite temperature on the GWDG cluster </li>
 <li> scripts/subJJGSparaContEffsec.sh : Starting p2TDVP for the polaron ground state on the GWDG cluster  </li>
 <li> scripts/subJJGSparaContEff.sh :  Continuing p2TDVP for the polaron ground state on the GWDG cluster </li>
 <li> scripts/subJJwithspinGSparaEffsec.sh :  Starting p2TDVP for the bipolaron for the ground state on the GWDG cluster </li>
 <li> scripts/subJJwithspinGSparaEffContsec.sh : Continuing p2TDVP for the bipolaron for the ground state on the GWDG cluster </li>
 <li> scripts/FT_test.py : Computing moments for the polaron at finite temperature </li>
 <li> scripts/FTwihspin_test.py : Computing moments for the bipolaron at finite temperature </li>
 <li> scripts/GSwithspinbipol_test.py Computing moments for the bipolaron for the ground state  </li>
 <li> scripts/GS_test.py Computing moments for the polaron in the ground state </li>
<ul>
<br />

## The repository dependes on the following other rerpositories

<ul>
 <li> [https://github.com/jansendavid/ITensor](https://github.com/jansendavid/ITensor): branch developer </li>
 <li> [https://gitlab.gwdg.de/jansen32/tensorft](https://gitlab.gwdg.de/jansen32/tensorft): for finite termpeature calculations, branch master </li>
 <li> [https://gitlab.gwdg.de/jansen32/tensortimeev](https://gitlab.gwdg.de/jansen32/tensortimeev): for time evolution methods, branch master </li>
  <li> [https://gitlab.gwdg.de/jansen32/tensortools](https://gitlab.gwdg.de/jansen32/tensortools): for the Holstein site sets and more, branch master </li>
    <li> [https://gitlab.gwdg.de/jansen32/parallel_tdvp_lbo](https://gitlab.gwdg.de/jansen32/parallel_tdvp_lbo): for parallel TDVP, branch master </li>
<ul>
