#include<iostream>
#include"basis.hpp"
#include"operators.hpp"
#include"diag.h"
#include"files.hpp"
#include"timeev.hpp"
#include"ETH.hpp"
#include"tpoperators.hpp"
#include <boost/program_options.hpp>
int main(int argc, char *argv[])
{
  using namespace Eigen;
using namespace std;
using namespace Many_Body;
  using Fermi_HubbardBasis= TensorProduct<ElectronBasis, ElectronBasis>;
   using HolsteinBasis= TensorProduct<ElectronBasis, PhononBasis>;
  using Mat= Operators::Mat;
  using boost::program_options::value;
   size_t M{};
  size_t L{};
  double t0{};
  double omega{};
  double gamma{};
  double dt{};
  double tot{};
    double T{};
  bool PB{};
  std::string dirname="";
  std::string filename={};
  
  try
  {
    boost::program_options::options_description desc{"Options"};
    desc.add_options()
      ("help,h", "Help screen")
      ("L", value(&L)->default_value(4), "L")
      ("M,m", value(&M)->default_value(2), "M")
      ("t0", value(&t0)->default_value(1.), "t0")
      ("gam", value(&gamma)->default_value(1.), "gamma")
      ("omg", value(&omega)->default_value(1.), "omega")
         ("T", value(&T)->default_value(0.1), "T")
      ("dir", value(&dirname)->default_value(""), "dirname")
    ("dt", value(&dt)->default_value(0.1), "dt")
      ("tot", value(&tot)->default_value(1.), "tot")
    ("pb", value(&PB)->default_value(true), "PB");
  


    boost::program_options::variables_map vm;
    boost::program_options::store(parse_command_line(argc, argv, desc), vm);
    boost::program_options::notify(vm);

  if (vm.count("help"))
      {std::cout << desc << '\n'; return 0;}
    else{
      if (vm.count("L"))
      {      std::cout << "L: " << vm["L"].as<size_t>() << '\n';
	filename+="L"+std::to_string(vm["L"].as<size_t>());
      }
     if (vm.count("M"))
      {
	std::cout << "M: " << vm["M"].as<size_t>() << '\n';
	filename+="M"+std::to_string(vm["M"].as<size_t>());
      }
      if (vm.count("t0"))
      {
	std::cout << "t0: " << vm["t0"].as<double>() << '\n';
	filename+="t0"+std::to_string(vm["t0"].as<double>()).substr(0, 3);
      }
       if (vm.count("omg"))
      {
	std::cout << "omega: " << vm["omg"].as<double>() << '\n';
	filename+="omega"+std::to_string(vm["omg"].as<double>()).substr(0, 3);
      }
       if (vm.count("gam"))
      {
	std::cout << "gamma: " << vm["gam"].as<double>() << '\n';
	filename+="gam"+std::to_string(vm["gam"].as<double>()).substr(0, 4);
      }
         if (vm.count("T"))
      {
	std::cout << "T: " << vm["T"].as<double>() << '\n';
	filename+="T"+std::to_string(vm["T"].as<double>()).substr(0, 4);
      }
       if (vm.count("dt"))
      {
	std::cout << "dt: " << vm["dt"].as<double>() << '\n';
	filename+="dt"+std::to_string(vm["dt"].as<double>()).substr(0, 4);
      }
       if (vm.count("tot"))
      {
	std::cout << "total time: " << vm["tot"].as<double>() << '\n';
	filename+="tot"+std::to_string(vm["tot"].as<double>()).substr(0, 3);
      }
       if (vm.count("pb"))
      {
	std::cout << "PB: " << vm["pb"].as<bool>() << '\n';
	filename+="PB"+std::to_string(vm["pb"].as<bool>());
      }
    }
  }
  catch (const boost::program_options::error &ex)
  {
    std::cerr << ex.what() << '\n';
    return 0;
  }



   ElectronState estate1(L, 1);
  ElectronBasis e( L, 1);

  PhononBasis ph(L, M);
  //         std::cout<< ph<<std::endl;
      HolsteinBasis TP(e, ph);
 
 		filename+=".bin";
	   


 		std::cout<< "dim "<< TP.dim << std::endl;  


 		Mat E1=Operators::EKinOperatorL(TP, e, 1, PB);
 		E1*=t0;
 		Mat Ebdag=Operators::NBosonCOperator(TP, ph, gamma, PB);
 		Mat Eb=Operators::NBosonDOperator(TP, ph, gamma, PB);
 		Mat Eph=Operators::NumberOperator(TP, ph, omega,  PB);
      
       Mat H=E1+Eph  +Ebdag + Eb;
  	   

   auto J=Operators::CurOperatorL(TP, e, 1, PB);    

 
       Eigen::VectorXd eigenVals(TP.dim);
        Eigen::VectorXd eigenVals2(TP.dim);
    
       Eigen::MatrixXd HH=Eigen::MatrixXd(H);
          Eigen::MatrixXd JJ=Eigen::MatrixXd(J);

 	  Eigen::MatrixXcd JJcp=JJ*std::complex<double>(0,1);
	  //std::cout<< HH<<std::endl;
              Many_Body::diagMat(HH, eigenVals);

	      //std::cout<< "EV  "<<std::endl<<eigenVals<<std::endl;
	 

  
    Eigen::MatrixXcd cEVec=HH.cast<std::complex<double>>();
    Eigen::VectorXcd newIn=HH.col(0);
      Eigen::VectorXcd newIn_L=HH.col(0);
    std::complex<double> E_in=(newIn.adjoint()*(H*newIn))(0);
    std::complex<double> E_k=(newIn.adjoint()*(E1*newIn))(0);
   std::complex<double> JJ_in=(newIn.adjoint()*(JJcp*newIn))(0);
    std::complex<double> JJsq_in=(newIn.adjoint()*(JJcp*JJcp*newIn))(0);

   //     std::cout<< " init energy "<<E_in<<std::endl;
   //     std::cout<< " init energy Kin "<<E_k<<std::endl;
   // std::cout<< " <J> "<<JJ_in<<std::endl;
   // std::cout<< " <J^2> "<<JJsq_in<<std::endl;

  int i=0;
  newIn=JJcp*newIn;
  std::vector<std::complex<double>> JJ_vec;
   std::vector<double> t_vec;
   Eigen::MatrixXcd JJmat=HH.adjoint()*(JJcp*HH);
   std::cout<< JJmat.cols()<< " and "<< JJmat.rows()<<std::endl;
     using std::exp;
    Eigen::VectorXcd expEigenVals=(-eigenVals/T);
    Eigen::MatrixXcd expEigenValsMatrix=Eigen::MatrixXcd::Zero(eigenVals.rows(), eigenVals.rows());
    Eigen::MatrixXcd JJcp2=HH.adjoint()*(JJcp*HH);
      Eigen::MatrixXcd E12=HH.adjoint()*(E1*HH);
      //   std::cout<< JJcp2<<std::endl;
      // std::cout<< JJcp2<<std::endl;
      //   std::cout<< E12<<std::endl;
        for (int i = 0; i < eigenVals.rows(); ++i)
       {
	 	 expEigenValsMatrix(i, i)=std::exp( expEigenVals(i) );
	 //expEigenValsMatrix.coeffRef(i, i)=std::exp(-(eigenVals(i)-eigenVals(0))*im*dt );	
}
	std::complex<double> val_1=(expEigenValsMatrix*JJcp2*JJcp2).trace();
		std::complex<double> val_2=(expEigenValsMatrix*E12).trace();
	std::complex<double> Z=expEigenValsMatrix.trace();
	std::cout<< "thermal JJ "<<val_1/Z<<std::endl;
		std::cout<< "thermal ek "<<val_2/Z<<std::endl;
 //        while(i*dt<tot)
 //       {
 // 	     Eigen::MatrixXcd evExp=TimeEv::EigenvalExponent(eigenVals, i*dt);
 //      Eigen::MatrixXcd evExp_MIN=TimeEv::EigenvalExponent(eigenVals, -i*dt);
 //      std::complex<double> val=(expEigenValsMatrix*evExp_MIN*JJcp2*evExp*JJcp2).trace()/Z;
 // 	 // 	 std::complex<double> val=(newIn_L.adjoint()*(JJcp*newIn))(0);
 // JJ_vec.push_back(val);
 //  t_vec.push_back(i*dt);
 // // 	 TimeEv::timeev_exact(newIn, cEVec, evExp);
 // // 	 	 TimeEv::timeev_exact(newIn_L, cEVec, evExp_MIN);
 //    	 i++;
 //  std::cout<< i*dt<<"  at "<<val <<std::endl;
 //       } 	   
 // 	   bin_write("JJHolex"+filename, JJ_vec);
 // 	     bin_write("JJMatHolex"+filename, JJmat);
 //  	     bin_write("EHolex"+filename, eigenVals);
 //  bin_write("timeHolex"+filename, t_vec);	
  

  return 0;
}
 
