#include "parallel_tdvp.h"
#include "itensor/all.h"
#include"files.hpp"
#include"holstein_withspin.hpp"
#include"makehamiltonians.hpp"
#include <iostream>
#include <iomanip>
#include<utility>
#include <boost/program_options.hpp>
#include <boost/filesystem.hpp>

using namespace itensor;
 const std::string obsname="JJ";


int
main(int argc, char* argv[])
    {
    Environment env(argc,argv);

    parallelDebugWait(env);

double cutoff{};
  double lbocutoff{};
  double phcutoff{};
  int M{};
  int j{};
  int Md{};
  int Mph{};
  int lboMd{};
  int L{};
     std::string somegap{};
std::string mpsNameB{};
std::string mpsNameK{};
 std::string siteSetName{};
  double t0{};
  double omega{};
  double gamma{};
  double dt{};
  double omegap{};
  double tot{};
 double t_in{};
 bool apply_op=false;
  std::string scutoff{};
   std::string sphcutoff{};
  std::string slbocutoff{};
  std::string sM{};
 std::string sj{};
  std::string sMph{};
  std::string sMd{};
  std::string slboMd{};
  std::string sL{};
  std::string st0{};
  std::string somega{};
  std::string sgamma{};
  std::string sdt{};
  std::string stot{};
std::string DIR{};


  std::string filename="";
  try
  {
    boost::program_options::options_description desc{"Options"};
    desc.add_options()
      ("help,h", "Help screen")
      ("appOP", boost::program_options::value(&apply_op)->default_value(false), "appOP")
      ("mpsNB", boost::program_options::value(&mpsNameB)->default_value("noName"), "mpsNB")
      ("mpsNK", boost::program_options::value(&mpsNameK)->default_value("noName"), "mpsNK")
      ("siteN", boost::program_options::value(&siteSetName)->default_value("noName"), "siteSetName")
      ("L", boost::program_options::value(&L)->default_value(4), "L")
      ("M", boost::program_options::value(&M)->default_value(4), "M")
      ("Md", boost::program_options::value(&Md)->default_value(2000), "Md")
      ("t0", boost::program_options::value(&t0)->default_value(1.0), "t0")
      ("omg", boost::program_options::value(&omega)->default_value(1.0), "omg")
      ("omgp", boost::program_options::value(&omegap)->default_value(0), "omgp")
      ("gam", boost::program_options::value(&gamma)->default_value(1.0), "gam")
      ("dt", boost::program_options::value(&dt)->default_value(0.01), "dt")
      ("tot", boost::program_options::value(&tot)->default_value(1.0), "tot")
      ("t_in", boost::program_options::value(&t_in)->default_value(0.0), "t_in")
      ("lbocut", boost::program_options::value(&lbocutoff)->default_value(1E-15), "lbocut")
      ("d", boost::program_options::value(&DIR)->default_value("NODIR"), "d")
          ("lboMd", boost::program_options::value(&lboMd)->default_value(100), "lboMd")
      ("cut", boost::program_options::value(&cutoff)->default_value(1E-15), "cut");

    boost::program_options::variables_map vm;
    boost::program_options::store(parse_command_line(argc, argv, desc), vm);
    boost::program_options::notify(vm);
    
    if (vm.count("help"))
      {std::cout << desc << '\n'; return 0;}
    else{
      if (vm.count("L"))
      {      std::cout << "L: " << vm["L"].as<int>() << '\n';
      	sL="L"+std::to_string(vm["L"].as<int>());
	filename+=sL;
      }
   
      	     if (vm.count("M"))
      {      std::cout << "M: " << vm["M"].as<int>() << '\n';
      	sM="M"+std::to_string(vm["M"].as<int>());
      	filename+=sM;
      }
	           	     if (vm.count("Md"))
      {      std::cout << "Md: " << vm["Md"].as<int>() << '\n';
      	sMd="Md"+std::to_string(vm["Md"].as<int>());
      	filename+=sMd;
      }
			           	     if (vm.count("lboMd"))
      {      std::cout << "lboMd: " << vm["lboMd"].as<int>() << '\n';
      	slboMd="lboMd"+std::to_string(vm["lboMd"].as<int>());
      	filename+=slboMd;
      }
      	 if (vm.count("t0"))
      {      std::cout << "t0: " << vm["t0"].as<double>() << '\n';
      	st0="t0"+std::to_string(vm["t0"].as<double>()).substr(0, 5);
      	filename+=st0;
      }
	 if (vm.count("gam"))
      {      std::cout << "gamma: " << gamma << '\n';
      	sgamma="gam"+std::to_string(vm["gam"].as<double>()).substr(0, 5);
      	filename+=sgamma;
      }
      	 	 if (vm.count("omg"))
      {      std::cout << "omega: " << vm["omg"].as<double>() << '\n';
      	somega="omega"+std::to_string(vm["omg"].as<double>()).substr(0, 5);
      		filename+=somega;
      }
		       	 	 if (vm.count("omgp"))
      {      std::cout << "omegap: " << vm["omgp"].as<double>() << '\n';
      	somegap="omegap"+std::to_string(vm["omgp"].as<double>()).substr(0, 5);
      		filename+=somegap;
      }
      		 if (vm.count("tot"))
      {      std::cout << "tot: " << vm["tot"].as<double>() << '\n';


      	stot="tot"+std::to_string(vm["tot"].as<double>()).substr(0, 6);

      		filename+=stot;
      }
      		 if (vm.count("dt"))
      {      std::cout << "dt: " << vm["dt"].as<double>() << '\n';
      	sdt="dt"+std::to_string(vm["dt"].as<double>()).substr(0, 6);
      		filename+=sdt;
      }
      		 if (vm.count("cut"))
      {      std::cout << "cutoff: " << vm["cut"].as<double>() << '\n';
	 std::stringstream ss;
	 ss<<vm["cut"].as<double>();
	 scutoff="cut"+ss.str();
      	filename+=scutoff;
      }
		 if (vm.count("lbocut"))
		   {
		std::cout << "lbocutoff: " << vm["lbocut"].as<double>() << '\n';
	 std::stringstream ss;
	 ss<<vm["lbocut"].as<double>();
	 slbocutoff="lbocut"+ss.str();
      	filename+=slbocutoff;
      }
	
      }
    }
  catch (const boost::program_options::error &ex)
  {
    std::cerr << ex.what() << '\n';
    return 0;
  }
  
  filename+="prcs"+std::to_string(env.nnodes());
       Holstein_withspin_one sites;
    MPO H1;
        MPO H2;
    MPS psi1;
      MPS psi2;
    Sweeps sweeps1;
    Sweeps sweeps2;
    double lbomaxB{0};
    double lbomaxK{0};
    MPO Curr;
  	 MPS psi1x;
  	 MPS psi2x;
       int N=L;
  
               auto argsMPS = itensor::Args("Cutoff=",cutoff,"MaxDimLBO",lboMd,"MinLBO",1,"CutoffLBO",lbocutoff  , "MaxDim=",Md, "MinDim=",10,"Truncate", true,"DoNormalize",false);
       	   auto argsObs= itensor::Args("Method=","DensityMatrix","MaxDim=",Md,"Cutoff=",cutoff);
    if(env.firstNode())
        {

	  Holstein_withspin_one sites = readFromFile<Holstein_withspin_one>(siteSetName);
   psi1 = readFromFile<MPS>(mpsNameK);
psi2 = readFromFile<MPS>(mpsNameB);
//   psi1.orthogonalize(argsMPS);
//   psi2.orthogonalize(argsMPS);
    auto curr= makeCurr_withspin(sites, t0);
  Curr = toMPO(curr);
  if(apply_op)
    { 
      auto y1 = applyMPO(Curr,psi1,argsObs);
     psi1=y1;
     psi1.noPrime();
      psi1x=psi1;
     	   psi2x=psi2;
	   write_data_GS(psi1x, psi2x, lbomaxK , lbomaxB, 0,Curr, argsObs, DIR, obsname,argsMPS);
    }
     sweeps1 = Sweeps(1);
    sweeps1.maxdim() = Md;
     sweeps1.mindim() = 1;
    sweeps1.cutoff() = cutoff;
    sweeps1.niter() = 10;
    sweeps2=sweeps1;
        auto am=makeHolstHam_disp_withspin(sites, t0, gamma, omega, omegap);
    H1 = toMPO(am);
        H2 = toMPO(am);

	   
	}
    auto  t_step=-dt*Cplx_i;



      env.broadcast(sites,H1,psi1,sweeps1);
      env.broadcast(sites,H2,psi2,sweeps2);

    bool first=true;
    Partition P1;
     std::vector<ITensor> Vs1;
           Partition P2;
     std::vector<ITensor> Vs2;
     Args args = Args::global();
         Observer obs;
	   splitWavefunction(env,psi1,P1,Vs1,argsMPS);
        splitWavefunction(env,psi2,P2,Vs2,argsMPS);
	 auto PH1 = computeHEnvironment(env,P1,psi1,Vs1,H1,argsMPS);
	 auto PH2 = computeHEnvironment(env,P2,psi2,Vs2,H2,argsMPS);
	 

	 int i=0;

     	   while(t_in +i*dt<tot)
      {

        	env.barrier();

		auto [lbomax_svdK, lbomax_solvK,lbomean_solvK]=Act_lboBT_secimpl(env,P1,psi1,H1,Vs1,PH1,sweeps1,obs,t_step, argsMPS, first);
		auto [lbomax_svdB, lbomax_solvB,lbomean_solvB]=Act_lboBT_secimpl(env,P2,psi2,H2,Vs2,PH2,sweeps2,obs,t_step, argsMPS, first);

       	    	if(first)
       	   {
   
            first=false;
       	   }
     	   	   i++;
	std::cout<< i << " and "<< t_in +i*dt << "  and "   << tot << "   and "  <<  (t_in +i*dt<tot)<<std::endl;     

	env.barrier();
           psi1x=psi1;
	    psi2x=psi2;
	     double final_lbosvdMaxB=gatherMax_val(env,lbomax_svdB);
	     double final_lbosvdMaxK=gatherMax_val(env,lbomax_svdK);
	     double final_lbosolvMaxB=gatherMax_val(env,lbomax_solvB);
	     double final_lbosolvMaxK=gatherMax_val(env,lbomax_solvK);
	     double final_lbosolvMeanB=gatherMax_val(env,lbomean_solvB);
	     double final_lbosolvMeanK=gatherMax_val(env,lbomean_solvK);
	     gather_vector(env,P1, psi1x, Vs1);

	     gather_vector(env,P2, psi2x, Vs2);
	     // making a copy and passing it as psix_2 since the function takes a refrence and I later want to store the wave function
	     // without having acted with J on it, not this copy should be cheap
	     
       	   if(env.firstNode())
       	    	     {
auto psi2x_cp=psi2x;
		       write_data_GS(psi1x, psi2x_cp, final_lbosvdMaxB , final_lbosvdMaxK, t_in+i*dt,Curr, argsObs, DIR, obsname,argsMPS);
		       std::vector<double> solvmeanK_vec={final_lbosolvMeanK};
		       std::vector<double> solvmeanB_vec={final_lbosolvMeanB};

		       std::vector<double> solvmaxK_vec={final_lbosolvMaxK};
		       std::vector<double> solvmaxB_vec={final_lbosolvMaxB};

    auto vecSize=solvmaxK_vec.size();
    			   
    itensor::ToFile(solvmeanK_vec, DIR+"/lbo_solvmeanK.dat", vecSize);
    itensor::ToFile(solvmeanB_vec, DIR+"/lbo_solvmeanB.dat", vecSize);

    itensor::ToFile(solvmaxK_vec, DIR+"/lbo_solvmxK.dat", vecSize);
    itensor::ToFile(solvmaxB_vec, DIR+"/lbo_solvmxB.dat", vecSize); 
       	   	     }


  
 

     	   }
 

       	   if(env.firstNode())
       	    	     {
		       write_state( psi1x, psi2x, mpsNameK,  mpsNameB,  DIR,  stot, false,apply_op);
    		     }

    return 0;
    }
