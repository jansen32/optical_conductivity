#include "parallel_tdvp.h"
#include "itensor/all.h"
#include"files.hpp"
#include"holstein_withspin.hpp"
#include "makebasis.hpp"
#include"makehamiltonians.hpp"
#include <iostream>
#include <iomanip>
#include<utility>
#include <boost/program_options.hpp>
#include <boost/filesystem.hpp>

using namespace itensor;

struct dbl_cmp {
    dbl_cmp(double v, double d) : val(v), delta(d) { }
    inline bool operator()(const double &x) const {
        return abs(x-val) < delta;
    }
  private:
    double val, delta;
};


int
main(int argc, char* argv[])
    {
    Environment env(argc,argv);

    parallelDebugWait(env);

     using boost::program_options::value;
   int M{};
   int Md{};
   int L{};
   double T{};
   double dt{};
   double t0{};
   double omega{};
    int lboMd{};
   double omegap{};
   double gamma{};
   double cutoff{};
 double lbocutoff{};
  std::string sM{};
  std::string sMd{};
  std::string sL{};
  std::string star{};
  std::string sT{};
  std::string st0{};
  std::string somega{};
  std::string somegap{};
  std::string sgamma{};
  std::string starget{};
  std::string scutoff{};
  std::string sPB{};
  std::string sdt;
  std::string stot;
    std::string slboMd{};
    std::string slbocutoff{};
     bool saveAll{};
  std::string filename="FTparawithspinbipoldisptdvplbo";
  try
  {
    boost::program_options::options_description desc{"Options"};
    desc.add_options()
      ("help,h", "Help screen")
      ("L", value(&L)->default_value(4), "L")
      ("T", value(&T)->default_value(0.1), "T")
      ("dt", value(&dt)->default_value(0.01), "dt")
      ("M,m", value(&M)->default_value(2), "M")
      ("Md", value(&Md)->default_value(3000), "Md")
      ("t0", value(&t0)->default_value(1.), "t0")
      ("gam", value(&gamma)->default_value(1.), "gam")
      ("omgp", value(&omegap)->default_value(0.), "omgp")
      ("lboMd", boost::program_options::value(&lboMd)->default_value(300), "lboMd")
       ("lbocut", boost::program_options::value(&lbocutoff)->default_value(1E-9), "lbocut")
      ("cut", boost::program_options::value(&cutoff)->default_value(1E-9), "cut")
      ("omg", value(&omega)->default_value(1.), "omg")
      ("SA", boost::program_options::value(&saveAll)->default_value(0), "SA");


      

  


    boost::program_options::variables_map vm;
    boost::program_options::store(parse_command_line(argc, argv, desc), vm);
    boost::program_options::notify(vm);

  if (vm.count("help"))
      {std::cout << desc << '\n'; return 0;}
    else{
      if (vm.count("L"))
      {      std::cout << "L: " << vm["L"].as<int>() << '\n';
      	sL="L"+std::to_string(vm["L"].as<int>());
	filename+=sL;
      }
         if (vm.count("M"))
      {      std::cout << "M: " << vm["M"].as<int>() << '\n';
      	sM="M"+std::to_string(vm["M"].as<int>());
	filename+=sM;
      }
      	 if (vm.count("t0"))
      {      std::cout << "t0: " << vm["t0"].as<double>() << '\n';
      	st0="t0"+std::to_string(vm["t0"].as<double>()).substr(0, 5);
      	filename+=st0;
      }
      	 	 if (vm.count("omg"))
      {      std::cout << "omega: " << vm["omg"].as<double>() << '\n';
      	somega="omg"+std::to_string(vm["omg"].as<double>()).substr(0, 5);
      		filename+=somega;
      }
		       	 	 if (vm.count("omgp"))
      {      std::cout << "omegap: " << vm["omgp"].as<double>() << '\n';
      	somegap="omgp"+std::to_string(vm["omgp"].as<double>()).substr(0, 5);
      		filename+=somegap;
      }
		 
      		 if (vm.count("gam"))
      {      std::cout << "gamma: " << vm["gam"].as<double>() << '\n';
      	sgamma="gam"+std::to_string(vm["gam"].as<double>()).substr(0, 5);
      		filename+=sgamma;
      }
		 if (vm.count("T"))
      {      std::cout << "T: " << vm["T"].as<double>() << '\n';
      	stot="T"+std::to_string(vm["T"].as<double>()).substr(0, 5);
      		filename+=stot;
      }
		       		 if (vm.count("dt"))
      {      std::cout << "dt: " << vm["dt"].as<double>() << '\n';
      	sdt="dt"+std::to_string(vm["dt"].as<double>()).substr(0, 6);
      		filename+=sdt;
      }
		 if (vm.count("cut"))
      {      std::cout << "cutoff: " << vm["cut"].as<double>() << '\n';
	 std::stringstream ss;
	 ss<<vm["cut"].as<double>();
	 scutoff="cut"+ss.str();
      	filename+=scutoff;
      }
		       if (vm.count("Md"))
      {      std::cout << "Md: " << vm["Md"].as<int>() << '\n';
      	sMd="Md"+std::to_string(vm["Md"].as<int>());
	filename+=sMd;
      }
		          {      std::cout << "lboMd: " << vm["lboMd"].as<int>() << '\n';
      	slboMd="lboMd"+std::to_string(vm["lboMd"].as<int>());
      	filename+=slboMd;
      }
			  		 if (vm.count("lbocut"))
		   {
		std::cout << "lbocutoff: " << vm["lbocut"].as<double>() << '\n';
	 std::stringstream ss;
	 ss<<vm["lbocut"].as<double>();
	 slbocutoff="lbocut"+ss.str();
      	filename+=slbocutoff;
      }
      }
  filename+="prcs"+std::to_string(env.nnodes());
    }
  catch (const boost::program_options::error &ex)
  {
    std::cerr << ex.what() << '\n';
    return 0;
  }
  filename+=".bin";
     
  
       int N=2*L;
       Holstein_exp_withspin sites = Holstein_exp_withspin(N,{"ConserveNf=",true,
                              "ConserveNb=",false,
       				"MaxOcc",M});
    MPO H;
        
    MPS psi;
      
    Sweeps sweeps;

    double lbomax{0};

    MPO Curr;
    MPO CurrSq;
    MPO NEL_UP;
    MPO NEL_DN;
    MPO NPH;
    MPO EK;
    MPO NX;
    std::vector<double> nph_vec;
    std::vector<double> temp_vec;
    std::vector<double> e_vec;
    std::vector<double> ek_vec;
    std::vector<double> nx_vec;
    std::vector<double> cur_vec;
    std::vector<double> cursq_vec;
    std::vector<double> maxlbo_vec;
    std::vector<double> maxbd_vec;
  	 MPS psix;

  double beta=1./T;  
               auto argsMPS = itensor::Args("Cutoff=",cutoff,"MaxDimLBO",lboMd,"MinLBO",1,"CutoffLBO",lbocutoff  , "MaxDim=",Md, "MinDim=",10,"Truncate", true,"DoNormalize",false ,"SVDMethod", "gesdd");
       	   auto argsObs= itensor::Args("Method=","DensityMatrix","MaxDim=",Md,"Cutoff=",cutoff);
    if(env.firstNode())
        {

          std::vector<int> v(N, M);
        psi=makeBasisNo_e2(sites, M);
	// making the super positions
	auto A=psi;

	{	std::vector<MPS> states;
	for(int i=1; i<=N; i+=2){
	  auto psi2=A;
   	  applyCdagup(psi2, i, sites);
  applyCdagup(psi2, i+1, sites);
  states.push_back(psi2);

}
   A = sum(states,{"MaxDim",Md,"Cutoff",cutoff});
    }
{	std::vector<MPS> states;
	for(int i=1; i<=N; i+=2){
	  auto psi2=A;
   	  applyCdagdn(psi2, i, sites);
  applyCdagdn(psi2, i+1, sites);
  states.push_back(psi2);

}
   A = sum(states,{"MaxDim",Md,"Cutoff",cutoff});
    }

	psi=A;
	psi.position(1);
	psi.normalize();


     sweeps = Sweeps(1);

    sweeps.maxdim() = Md;
     sweeps.mindim() = 1;
    sweeps.cutoff() = cutoff;
     int Niter=80;  
    sweeps.niter() = Niter;
    
    auto ampo=makeHolstHam_dispFT_withspin(sites, t0, gamma, omega, omegap);
    H = toMPO(ampo);
 AutoMPO Nel_up(sites);
 AutoMPO Nel_dn(sites);
  AutoMPO Nph(sites);
      AutoMPO Ek(sites);
    AutoMPO x(sites);
   auto curr= makeCurr_FT_withspin(sites, t0);
  

    for(int b = 1; b <=N ; b+=2)
      {

Nel_up += 1,"Nup",b ;
Nel_dn += 1,"Ndn",b ;
  x += 1,"NtotX",b ;
 Nph += 1,"Nph",b;


      }
        for(int b = 1; b <N-1 ; b+=2)
      {

	Ek += -t0,"Cdagup",b, "Cup", b+2 ;
	Ek += -t0,"Cdagup",b+2, "Cup", b ;
	Ek += -t0,"Cdagdn",b, "Cdn", b+2 ;
	Ek += -t0,"Cdagdn",b+2, "Cdn", b ;

}
	EK = toMPO(Ek);
   NPH = toMPO(Nph);;
   NX = toMPO(x);;
   NEL_UP = toMPO(Nel_up);
   NEL_DN = toMPO(Nel_dn);
   Curr = toMPO(curr);

    auto nph=innerC(psi, NPH, psi)/innerC(psi, psi);
     auto ne_up=innerC(psi, NEL_UP, psi)/innerC(psi, psi);
     auto ne_dn=innerC(psi, NEL_DN, psi)/innerC(psi, psi);
 auto ex=innerC(psi, H, psi)/innerC(psi, psi);
   
      printfln("\n N/L %.4f %.20f",0, nph/L);
       printfln("\n NE up /L %.4f %.20f",0, ne_up/L);
printfln("\n NE dn /L %.4f %.20f",0, ne_dn/L);
       printfln("\n E/L %.4f %.20f",0, ex/L);
	}
   double beta2=0;
     auto  t_step=-dt*0.5;



     env.broadcast(sites,H,psi,sweeps);

    bool first=true;

     Partition P;
      std::vector<ITensor> Vs;

          Observer obs;
     	   splitWavefunction(env,psi,P,Vs,argsMPS);

     	 auto PH = computeHEnvironment(env,P,psi,Vs,H,argsMPS);
std::vector<double> Tsstore={1.0,0.4,0.20, 0.1};
	 

    
    auto nt = int(beta/dt+(1e-9*(beta/dt)));
  for(int i=1; i<=nt; ++i)
        {

	  if(i%10==0)
	    {
	   if(env.firstNode())
	     {
	       // psi.position(1);
	       //	       psi.normalize();
}
    parallelDebugWait(env);
     env.broadcast(sites,H,psi,sweeps);
     //   P=Partition();
     //	   Vs=std::vector<ITensor>();
     	   splitWavefunction(env,psi,P,Vs,argsMPS);

     	  PH = computeHEnvironment(env,P,psi,Vs,H,argsMPS);
}

//          	env.barrier();

            	 		double lbomax_svd=Act_lboBT(env,P,psi,Vs,PH,sweeps,obs,t_step, argsMPS, first);


        	    	if(first)
        	   {
   
             first=false;
        	   }
    
    // 	std::cout<< i << " and "<< t_in +i*dt << "  and "   << tot << "   and "  <<  (t_in +i*dt<tot)<<std::endl;     

     	env.barrier();
            psix=psi;

	        	     double final_lboMax=gatherMax_val(env,lbomax_svd);

     	     gather_vector(env,P, psix, Vs);


    // 	     // making a copy and passing it as psix_2 since the function takes a refrence and I later want to store the wave function

    
        	   if(env.firstNode())
        	    	     {
	beta2+=dt;
 			       double norm=inner(psix, psix);
        auto en=innerC(psix, H, psix);
       auto nph=innerC(psix, NPH, psix);
       auto ek=innerC(psix, EK, psix);
       auto smNx=innerC(psix, NX, psix);
              auto ne_up=innerC(psix, NEL_UP, psix);
         auto ne_dn=innerC(psix, NEL_DN, psix);
        auto curval=innerC(psix, Curr, psix);
       	  nph_vec.push_back(real(nph)/norm);
       	  e_vec.push_back(real(en)/norm);
       	  nx_vec.push_back(real(smNx)/norm);
       	  ek_vec.push_back(real(ek)/norm);
			     cur_vec.push_back(real(curval)/norm);
       	  maxbd_vec.push_back(maxLinkDim(psix));
       	  maxlbo_vec.push_back(final_lboMax);
       // auto y1 = applyMPO(Curr,psir2,argsMPO);
       //   y1.noPrime();    
       // 	 auto curSQval=innerC(y1, y1);
	  //	 curSQvec.push_back(real(curSQval));
		  //	 curSQvec.push_back(0);
     	  temp_vec.push_back(1./beta2);  	  	       
 	  printfln("\n at T/ omega Energy/L %.4f %.20f",1./(beta2*omega), en/(norm*L));
	  printfln("\n wt beta omega Nph/L %.4f %.20f",i*dt*omega, nph/(norm*L));
	  std::cout<< "com "<< real(en)/norm << " max BD "<< maxLinkDim(psix)  << " lbo max "<< final_lboMax<< "  electron number up "<< ne_up/norm<<"  electron number dn "<< ne_dn/norm<< " and norm " << norm<<std::endl;        	   	     
if(saveAll && (std::find_if(Tsstore.begin(), Tsstore.end(), dbl_cmp((1./beta2), 1E-8))!=Tsstore.end()))
		    {

 std::string SC="aT"+std::to_string(1./beta2).substr(0, 4);
//		      Print(psir2);
		      itensor::writeToFile("MPS"+SC+filename,psix);
		      itensor::writeToFile("siteset"+SC+filename,sites);
		     
    double s_up{0};
     double s2_up{0};
    double s_dn{0};
     double s2_dn{0};
   for(int b = 1; b <= N; b+=1)
        {
	  psix.position(b);
   	     auto bb = AutoMPO(sites);
  
   	     bb += 1,"Ntot",b, "B",b ;
   	     bb += 1,"Ntot",b, "Bdag",b ;
   		      auto BB = toMPO(bb);
   		      auto en3 = innerC(psix,BB,psix)/norm;
        printfln("X %d %.12f",b,en3);
   		      auto gg = AutoMPO(sites);
  
   		        gg += 1,"Nph",b ;
   		      auto GG = toMPO(gg);
   		      auto en2 = innerC(psix,GG,psix)/norm;
   		         printfln("Nph %d %.12f",b,en2);
   			 auto gg2 = AutoMPO(sites);
  
   		        gg2 += 1,"Nup",b ;
   		      auto GG2 = toMPO(gg2);
   		      auto en22 = innerC(psix,GG2,psix)/norm;
   		         printfln("Ne up %d %.12f",b,en22);
			 

   			 auto gg3 = AutoMPO(sites);
			 gg3 += 1,"Ndn",b ;
   		      auto GG3 = toMPO(gg2);
   		      auto en33 = innerC(psix,GG3,psix)/norm;
   		         printfln("Ne dn %d %.12f",b,en33);
			 if((b+1)%2==0)
			   {
			 s_up+=real(en22);
			 s_dn+=real(en33);
			   }
			 else{
			   s2_up+=real(en22);
			   s2_dn+=real(en33);
			 }

	}
		    }
	 
      }



       	   }
 

        	   if(env.firstNode())
        	    	     {
        auto psi2=psi;

	 // std::cout<< " f1 "<< innerC(psi1,Ne, psi2)<<'\n';
    double s_up{0};
     double s2_up{0};
    double s_dn{0};
     double s2_dn{0};
   for(int b = 1; b <= N; b+=1)
        {
	  psi2.position(b);
   	     auto bb = AutoMPO(sites);
  
   	     bb += 1,"Ntot",b, "B",b ;
   	     bb += 1,"Ntot",b, "Bdag",b ;
   		      auto BB = toMPO(bb);
   		      auto en3 = innerC(psi2,BB,psi2)/innerC(psi2, psi2);
        printfln("X %d %.12f",b,en3);
   		      auto gg = AutoMPO(sites);
  
   		        gg += 1,"Nph",b ;
   		      auto GG = toMPO(gg);
   		      auto en2 = innerC(psi2,GG,psi2)/innerC(psi2, psi2);
   		         printfln("Nph %d %.12f",b,en2);
   			 auto gg2 = AutoMPO(sites);
    			 
  
   		        gg2 += 1,"Nup",b ;
   		      auto GG2 = toMPO(gg2);
   		      auto en22 = innerC(psi2,GG2,psi2)/innerC(psi2, psi2);
   		         printfln("Ne up %d %.12f",b,en22);
			 

   			 auto gg3 = AutoMPO(sites);
			 gg3 += 1,"Ndn",b ;
   		      auto GG3 = toMPO(gg2);
   		      auto en33 = innerC(psi2,GG3,psi2)/innerC(psi2, psi2);
   		         printfln("Ne dn %d %.12f",b,en33);
			 if((b+1)%2==0)
			   {
			 s_up+=real(en22);
			 s_dn+=real(en33);
			   }
			 else{
			   s2_up+=real(en22);
			   s2_dn+=real(en33);
			 }
   


   }
   std::cout<< "end sum was in phys was up "<<s_up << " and dn " << s_dn<<std::endl;
   std::cout<< "end sum was in aux up "<<s2_up << " and dn "<< s2_dn<<std::endl;
  		 itensor::bin_write("temp"+filename, temp_vec);
  		 itensor::bin_write("nX"+filename, nx_vec);
		 itensor::bin_write("J"+filename, cur_vec);
		 //		 itensor::bin_write("JJ"+filename, curSQvec);
  		 itensor::bin_write("EK"+filename, ek_vec);
  		 itensor::bin_write("Nph"+filename, nph_vec);
  		 itensor::bin_write("E"+filename, e_vec);
		 itensor::bin_write("maxLBO_svd"+filename, maxlbo_vec);
		 itensor::bin_write("mxBD"+filename, maxbd_vec);
		

     		     }

    return 0;
    }
