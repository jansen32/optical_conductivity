
#include "itensor/all.h"
#include"holstein.hpp"
#include"makehamiltonians.hpp"
#include <iostream>
#include <iomanip>
#include<utility>
#include <boost/program_options.hpp>
#include <boost/filesystem.hpp>
#include"files.hpp"
using namespace std;
using namespace itensor;

int main(int argc, char *argv[])
{
  
  double cutoff{};
  double lbocutoff{};
  int M{};
  int j{};
  int Md{};
  int lboMd{};
  int L{};
     std::string somegap{};
std::string mpsName{};
 std::string siteSetName{};
  double t0{};
  double omega{};
  double gamma{};
  double dt{};
  double omegap{};
  double tot{};

  std::string scutoff{};
  std::string slbocutoff{};
  std::string sM{};
  std::string sMd{};
  std::string slboMd{};
  std::string sL{};
  std::string st0{};
  std::string somega{};
  std::string sgamma{};
  std::string sdt{};
  std::string stot{};


  std::string filename="";
  try
  {
    boost::program_options::options_description desc{"Options"};
    desc.add_options()
      ("help,h", "Help screen")
      ("mpsN", boost::program_options::value(&mpsName)->default_value("noName"), "mpsN")
      ("siteN", boost::program_options::value(&siteSetName)->default_value("noName"), "siteSetName")
      ("L", boost::program_options::value(&L)->default_value(4), "L")
      ("M", boost::program_options::value(&M)->default_value(4), "M")
      ("Md", boost::program_options::value(&Md)->default_value(3000), "Md")
      ("lboMd", boost::program_options::value(&lboMd)->default_value(100), "lboMd")
      ("t0", boost::program_options::value(&t0)->default_value(1.0), "t0")
      ("omg", boost::program_options::value(&omega)->default_value(1.0), "omg")
               ("omgp", boost::program_options::value(&omegap)->default_value(0), "omgp")
      ("gam", boost::program_options::value(&gamma)->default_value(1.0), "gam")
      ("dt", boost::program_options::value(&dt)->default_value(0.01), "dt")
      ("tot", boost::program_options::value(&tot)->default_value(1.0), "tot")
      ("lbocut", boost::program_options::value(&lbocutoff)->default_value(1E-15), "lbocut")
      ("cut", boost::program_options::value(&cutoff)->default_value(1E-15), "cut");

    boost::program_options::variables_map vm;
    boost::program_options::store(parse_command_line(argc, argv, desc), vm);
    boost::program_options::notify(vm);
    
    if (vm.count("help"))
      {std::cout << desc << '\n'; return 0;}
    else{
      if (vm.count("L"))
      {      std::cout << "L: " << vm["L"].as<int>() << '\n';
      	sL="L"+std::to_string(vm["L"].as<int>());
	filename+=sL;
      }
   
      	     if (vm.count("M"))
      {      std::cout << "M: " << vm["M"].as<int>() << '\n';
      	sM="M"+std::to_string(vm["M"].as<int>());
      	filename+=sM;
      }
	           	     if (vm.count("Md"))
      {      std::cout << "Md: " << vm["Md"].as<int>() << '\n';
      	sMd="Md"+std::to_string(vm["Md"].as<int>());
      	filename+=sMd;
      }
			           	     if (vm.count("M"))
      {      std::cout << "lboMd: " << vm["lboMd"].as<int>() << '\n';
      	slboMd="lboMd"+std::to_string(vm["lboMd"].as<int>());
      	filename+=slboMd;
      }
      	 if (vm.count("t0"))
      {      std::cout << "t0: " << vm["t0"].as<double>() << '\n';
      	st0="t0"+std::to_string(vm["t0"].as<double>()).substr(0, 3);
      	filename+=st0;
      }
	 if (vm.count("gam"))
      {      std::cout << "gamma: " << gamma << '\n';
      	sgamma="gam"+std::to_string(vm["gam"].as<double>()).substr(0, 3);
      	filename+=sgamma;
      }
      	 	 if (vm.count("omg"))
      {      std::cout << "omega: " << vm["omg"].as<double>() << '\n';
      	somega="omega"+std::to_string(vm["omg"].as<double>()).substr(0, 3);
      		filename+=somega;
      }
		       	 	 if (vm.count("omgp"))
      {      std::cout << "omegap: " << vm["omgp"].as<double>() << '\n';
      	somegap="omegap"+std::to_string(vm["omgp"].as<double>()).substr(0, 3);
      		filename+=somegap;
      }
      		 if (vm.count("tot"))
      {      std::cout << "tot: " << vm["tot"].as<double>() << '\n';
      	stot="tot"+std::to_string(vm["tot"].as<double>()).substr(0, 3);
      		filename+=stot;
      }
      		 if (vm.count("dt"))
      {      std::cout << "dt: " << vm["dt"].as<double>() << '\n';
      	sdt="dt"+std::to_string(vm["dt"].as<double>()).substr(0, 6);
      		filename+=sdt;
      }
      		 if (vm.count("cut"))
      {      std::cout << "cutoff: " << vm["cut"].as<double>() << '\n';
	 std::stringstream ss;
	 ss<<vm["cut"].as<double>();
	 scutoff="cut"+ss.str();
      	filename+=scutoff;
      }
		 if (vm.count("lbocut"))
		   {
		std::cout << "lbocutoff: " << vm["lbocut"].as<double>() << '\n';
	 std::stringstream ss;
	 ss<<vm["lbocut"].as<double>();
	 slbocutoff="lbocut"+ss.str();
      	filename+=slbocutoff;
      }

      }
    }
  catch (const boost::program_options::error &ex)
  {
    std::cerr << ex.what() << '\n';
    return 0;
  }
 using Holstein_exp = MixedSiteSet<HolsteinSite_down,HolsteinSite_up>;
   auto sites = readFromFile<Holstein_exp>(siteSetName);
    auto psi = readFromFile<MPS>(mpsName);
    /// 2L since we are at finite T
  int N=2*L;
    std::vector<double> stateDens;
    for(int i=1; i<length(psi); i+=2)
      {
        psi.position(i);

        auto ket = psi(i);
        auto bra = dag(prime(ket,"Site"));

        auto niop = op(sites,"n",i);

        //take an inner product 
        auto ni = elt(bra*niop*ket);

	stateDens.push_back(ni);
}
    itensor::bin_write("densities"+mpsName, stateDens);
  //auto argsObs= itensor::Args("Method=","DensityMatrix","MaxDim=",Md,"Cutoff=",cutoff);
    auto argsObs= itensor::Args("Method=","DensityMatrix","MaxDim=",Md,"Cutoff=",cutoff);
      auto ek= makeE_kin_FT(sites, t0);
    auto EKIN = toMPO(ek);

     auto curr= makeCurr_FT(sites, t0);
     auto Curr = toMPO(curr, {"Exact=",true});
        auto curr_col1_mom= CurCorr_momentum1_left_FT(sites, t0, gamma);
	auto Curr_col1_mom = toMPO(curr_col1_mom);

	auto curr_col2_mom= CurCorr_momentum2_left_FT(sites, t0, gamma, omega, omegap);
	auto Curr_col2_mom = toMPO(curr_col2_mom);
    auto am=makeHolstHam_dispFT(sites, t0, gamma, omega, omegap);
    auto H = toMPO(am);
    psi.position(1);
    std::cout<< "norm "<<norm(psi)<< " maxc bD "<< maxLinkDim(psi)<<std::endl; 
	      std::cout<< "start energy "<<innerC(psi,H, psi)<<std::endl;
std::cout<< "start kin energy "<<innerC(psi,EKIN, psi)<<std::endl;
	      std::cout<< "start obs "<<innerC(psi,Curr, psi)<<std::endl;
	      auto y1 = applyMPO(Curr,psi,argsObs);
	       y1.noPrime();
std::cout<< "start obs sqer "<<innerC(psi,Curr, y1)<<std::endl;
	         auto y1x = applyMPO(Curr_col1_mom,psi,argsObs);
   y1x.noPrime();

 std::cout<< "start obs first mom "<<innerC(y1, y1x)<<std::endl;	       
	         auto y2x = applyMPO(Curr_col2_mom,psi,argsObs);
   y2x.noPrime();



 std::cout<< "start obs sec mom "<<innerC(y1,y2x)<<std::endl;
 std::cout<< "n "<< norm(y1)<<std::endl;
  return 0;
}
