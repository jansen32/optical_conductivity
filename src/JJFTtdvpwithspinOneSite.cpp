
#include "itensor/all.h"
#include"files.hpp"
#include"holstein_withspin.hpp"
#include"makehamiltonians.hpp"
#include <iostream>
#include <iomanip>
#include<utility>
#include <boost/program_options.hpp>
#include"write_data.h"
#include"tdvp.hpp"
#include <boost/filesystem.hpp>
using namespace std;
using namespace itensor;

int main(int argc, char *argv[])
{
  
  double cutoff{};

  int M{};
  int Md{};

  int L{};
     std::string somegap{};
std::string mpsNameB{};
std::string mpsNameK{};
 std::string siteSetName{};
  double t0{};
  double omega{};
  double gamma{};
  double dt{};
  double omegap{};
  double tot{};
  double t_in{};
  std::string DIR{};
 bool apply_op=false;
  std::string stot{};


  std::string filename="";
  try
  {
    boost::program_options::options_description desc{"Options"};
    desc.add_options()
      ("help,h", "Help screen")
      ("mpsNB", boost::program_options::value(&mpsNameB)->default_value("noName"), "mpsNB")
      ("mpsNK", boost::program_options::value(&mpsNameK)->default_value("noName"), "mpsNK")
      ("siteN", boost::program_options::value(&siteSetName)->default_value("noName"), "siteSetName")
      ("appOP", boost::program_options::value(&apply_op)->default_value(false), "appOP")
      ("L", boost::program_options::value(&L)->default_value(4), "L")
      ("M", boost::program_options::value(&M)->default_value(4), "M")
      ("Md", boost::program_options::value(&Md)->default_value(3000), "Md")
      ("t0", boost::program_options::value(&t0)->default_value(1.0), "t0")
      ("omg", boost::program_options::value(&omega)->default_value(1.0), "omg")
      ("omgp", boost::program_options::value(&omegap)->default_value(0), "omgp")
      ("gam", boost::program_options::value(&gamma)->default_value(1.0), "gam")
      ("dt", boost::program_options::value(&dt)->default_value(0.01), "dt")
      ("tot", boost::program_options::value(&tot)->default_value(1.0), "tot")
      ("t_in", boost::program_options::value(&t_in)->default_value(0.0), "t_in")
      ("d", boost::program_options::value(&DIR)->default_value("NONAME"), "d")
      ("cut", boost::program_options::value(&cutoff)->default_value(1E-15), "cut");

    boost::program_options::variables_map vm;
    boost::program_options::store(parse_command_line(argc, argv, desc), vm);
    boost::program_options::notify(vm);
    
    if (vm.count("help"))
      {std::cout << desc << '\n'; return 0;}
    else{
      if (vm.count("L"))
      {      std::cout << "L: " << vm["L"].as<int>() << '\n';

      	

      }
   
      	     if (vm.count("M"))
      {      std::cout << "M: " << vm["M"].as<int>() << '\n';
      
      
      }
	     if (vm.count("Md"))
	       {      std::cout << "Md: " << vm["Md"].as<int>() << '\n';


	       }
	 
      	 if (vm.count("t0"))
      {      std::cout << "t0: " << vm["t0"].as<double>() << '\n';
      
      
      }
	 if (vm.count("gam"))
      {      std::cout << "gamma: " << gamma << '\n';
      
      
      }
      	 	 if (vm.count("omg"))
      {      std::cout << "omega: " << vm["omg"].as<double>() << '\n';
      
      
      }
		       	 	 if (vm.count("omgp"))
      {      std::cout << "omegap: " << vm["omgp"].as<double>() << '\n';
      
      
      }
      		 if (vm.count("tot"))
      {      std::cout << "tot: " << vm["tot"].as<double>() << '\n';
      	stot="tot"+std::to_string(vm["tot"].as<double>()).substr(0, 6);
      
      }
      		 if (vm.count("dt"))
      {      std::cout << "dt: " << vm["dt"].as<double>() << '\n';
      
      
      }
      		 if (vm.count("cut"))
      {      std::cout << "cutoff: " << vm["cut"].as<double>() << '\n';


      }

      }
    }
  catch (const boost::program_options::error &ex)
  {
    std::cerr << ex.what() << '\n';
    return 0;
  }

   auto sites = readFromFile<Holstein_exp_withspin>(siteSetName);
    auto psi1 = readFromFile<MPS>(mpsNameK);
   auto psi2 = readFromFile<MPS>(mpsNameB);
  int N=L;
   


  auto argsMPS = itensor::Args("Cutoff=",cutoff,"MaxDim=",Md, "Truncate", false,"DoNormalize",false, "SVDMethod", "gesdd","NumCenter",1,"Silent",true);



   std::chrono::duration<double> elapsed_seconds;
 std::chrono::time_point<std::chrono::system_clock> start, end;
   

    auto sweeps = Sweeps(1);
    sweeps.maxdim() = Md;
    sweeps.cutoff() = cutoff;
    sweeps.niter() = 10;
    println(sweeps);
    auto sweeps_1=sweeps;
    auto sweeps_2=sweeps;
    auto am=makeHolstHam_dispFTAux_withspin(sites, t0, gamma, omega, omegap);
    auto H1 = toMPO(am);
auto H2 = toMPO(am);
    auto argsObs= itensor::Args("Method=","DensityMatrix","MaxDim=",Md, "Cutoff=",cutoff);
    double lbomax=0;
   std::string obsname="JJ";
    if(apply_op)
      {
   auto curr= makeCurr_FT_withspin(sites, t0);
    auto Curr = toMPO(curr);


	      auto y1 = applyMPO(Curr,psi1,argsObs);

	      y1.noPrime();
	     
 psi1=y1;

   psi2=y1;

     write_data_FT(psi1, psi2, lbomax, lbomax, 0, argsObs, DIR,obsname, argsMPS);
      }
    auto  t_step=-0.5*dt*Cplx_i;
    TDVP<MPS> tdvp1(psi1,H1, t_step, sweeps_1,argsMPS);
    TDVP<MPS> tdvp2(psi2,H2,-t_step,sweeps_2,argsMPS);

    //    std::cout<< "start energy "<<innerC(psi1,H1, psi1)<<std::endl;
    //	      	      std::cout<< "start obs "<<innerC(psi2,Curr, psi)<<std::endl;
	 
 


	      int i=0;


      	   while(t_in + i*dt<tot)
      {

	      
	      tdvp1.step_new(sweeps_1);
	      
	      	      tdvp2.step_new(sweeps_2);
	     
	
		      i++;
	      	       write_data_FT(psi1, psi2, lbomax , lbomax, t_in +i*dt, argsObs, DIR, obsname, argsMPS);
		    
		      if(i%100==0){

				std::vector<double> en_vec_1={real(innerC(psi1,H1, psi1))};
				std::vector<double> en_vec_2={real(innerC(psi2,H2, psi2))};

			   auto vecSize=en_vec_1.size();
    			   
			itensor::ToFile(en_vec_1, DIR+"/energy_1.dat", vecSize);
			   itensor::ToFile(en_vec_2, DIR+"/energy_2.dat", vecSize);
		      }

      	   	     }

		      		       write_state( psi1, psi2, mpsNameK,  mpsNameB,  DIR,  stot, apply_op,apply_op);

  return 0;
}
