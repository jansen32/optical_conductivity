#include<iostream>
#define EIGEN_USE_MKL_ALL
#include"basis.hpp"
#include"operators.hpp"
#include"diag.h"
#include"files.hpp"
#include"timeev.hpp"
#include"ETH.hpp"
#include"tpoperators.hpp"
#include <boost/program_options.hpp>
int main(int argc, char *argv[])
{
  using namespace Eigen;
using namespace std;
using namespace Many_Body;
  using Fermi_HubbardBasis= TensorProduct<ElectronBasis, ElectronBasis>;
   using HolsteinBasis= TensorProduct<ElectronBasis, PhononBasis>;
  using Mat= Operators::Mat;
  using boost::program_options::value;
   size_t M{};
  size_t L{};
  double t0{};
  double omega{};
  double omegap{};
  double gamma{};
  double dt{};
  double tot{};
  bool PB{};
  std::string dirname="";
  std::string filename={};
  
  try
  {
    boost::program_options::options_description desc{"Options"};
    desc.add_options()
      ("help,h", "Help screen")
      ("L", value(&L)->default_value(4), "L")
      ("M,m", value(&M)->default_value(2), "M")
      ("t0", value(&t0)->default_value(1.), "t0")
      ("gam", value(&gamma)->default_value(1.), "gamma")
      ("omg", value(&omega)->default_value(1.), "omega")
      ("omgp", value(&omegap)->default_value(1.), "omegap")
      ("dir", value(&dirname)->default_value(""), "dirname")
    ("dt", value(&dt)->default_value(0.1), "dt")
      ("tot", value(&tot)->default_value(1.), "tot")
    ("pb", value(&PB)->default_value(true), "PB");
  


    boost::program_options::variables_map vm;
    boost::program_options::store(parse_command_line(argc, argv, desc), vm);
    boost::program_options::notify(vm);

  if (vm.count("help"))
      {std::cout << desc << '\n'; return 0;}
    else{
      if (vm.count("L"))
      {      std::cout << "L: " << vm["L"].as<size_t>() << '\n';
	filename+="L"+std::to_string(vm["L"].as<size_t>());
      }
     if (vm.count("M"))
      {
	std::cout << "M: " << vm["M"].as<size_t>() << '\n';
	filename+="M"+std::to_string(vm["M"].as<size_t>());
      }
      if (vm.count("t0"))
      {
	std::cout << "t0: " << vm["t0"].as<double>() << '\n';
	filename+="t0"+std::to_string(vm["t0"].as<double>()).substr(0, 6);
      }
       if (vm.count("omg"))
      {
	std::cout << "omega: " << vm["omg"].as<double>() << '\n';
	filename+="omega"+std::to_string(vm["omg"].as<double>()).substr(0, 6);
      }
       if (vm.count("omgp"))
      {
	std::cout << "omegap: " << vm["omgp"].as<double>() << '\n';
	filename+="omgp"+std::to_string(vm["omgp"].as<double>()).substr(0, 6);
      }
       if (vm.count("gam"))
      {
	std::cout << "gamma: " << vm["gam"].as<double>() << '\n';
	filename+="gam"+std::to_string(vm["gam"].as<double>()).substr(0, 6);
      }
       if (vm.count("dt"))
      {
	std::cout << "dt: " << vm["dt"].as<double>() << '\n';
	filename+="dt"+std::to_string(vm["dt"].as<double>()).substr(0, 6);
      }
       if (vm.count("tot"))
      {
	std::cout << "total time: " << vm["tot"].as<double>() << '\n';
	filename+="tot"+std::to_string(vm["tot"].as<double>()).substr(0, 6);
      }

       if (vm.count("pb"))
      {
	std::cout << "PB: " << vm["pb"].as<bool>() << '\n';
	filename+="PB"+std::to_string(vm["pb"].as<bool>());
      }
    }
  }
  catch (const boost::program_options::error &ex)
  {
    std::cerr << ex.what() << '\n';
    return 0;
  }



   ElectronState estate1(L, 1);
  ElectronBasis e( L, 1);

  PhononBasis ph(L, M);
  //         std::cout<< ph<<std::endl;
      HolsteinBasis TP(e, ph);
 
 		filename+=".bin";
	   


 		std::cout<< "dim "<< TP.dim << std::endl;  


 		Mat E1=Operators::EKinOperatorL(TP, e, t0, PB);

 		Mat Ebdag=Operators::NBosonCOperator(TP, ph, gamma, PB);
 		Mat Eb=Operators::NBosonDOperator(TP, ph, gamma, PB);
 		Mat Eph=Operators::NumberOperator(TP, ph, omega,  PB);
		Mat EDI=Operators::EKinOperatorRPH(TP, ph,omegap, PB);      
       Mat H=E1+Eph  +Ebdag + Eb+EDI;
  	   

   auto J=Operators::CurOperatorL(TP, e, t0, PB);    

 
       Eigen::VectorXd eigenVals(TP.dim);
        Eigen::VectorXd eigenVals2(TP.dim);
    
       Eigen::MatrixXd HH=Eigen::MatrixXd(H);
          Eigen::MatrixXd JJ=Eigen::MatrixXd(J);

 	  Eigen::MatrixXcd JJcp=JJ*std::complex<double>(0,1);
	  //std::cout<< HH<<std::endl;
              Many_Body::diagMat(HH, eigenVals);

	      std::cout<< "EV  "<<std::endl<<eigenVals(0)<<std::endl;
	      Eigen::MatrixXcd HH_cp=HH.cast<std::complex<double>>();

      Eigen::SparseMatrix<std::complex<double>, Eigen::RowMajor> evExp=TimeEv::EigenvalExponent(eigenVals, dt);
      Eigen::SparseMatrix<std::complex<double>, Eigen::RowMajor> evExp_MIN=TimeEv::EigenvalExponent(eigenVals, dt);
      Eigen::MatrixXcd cEVec=HH_cp;
    Eigen::VectorXcd newIn=HH_cp.col(0);
      Eigen::VectorXcd newIn_L=HH_cp.col(0);
    std::complex<double> E_in=(newIn.adjoint()*(H*newIn))(0);
    std::complex<double> E_k=(newIn.adjoint()*(E1*newIn))(0);
newIn=JJcp.selfadjointView<Eigen::Lower>()*newIn;
    std::complex<double> JJ_in=(newIn.adjoint()*newIn)(0);
    std::complex<double> JJsq_in=(newIn.adjoint()*(JJcp.selfadjointView<Eigen::Lower>()*newIn))(0);

       std::cout<< " init energy "<<E_in<<std::endl;
       std::cout<< " init energy Kin "<<E_k<<std::endl;
   std::cout<< " <J> "<<JJ_in<<std::endl;
   std::cout<< " <J^2> "<<JJsq_in<<std::endl;

  int i=0;
  


   Eigen::MatrixXcd JJmat=HH_cp.adjoint()*(JJcp.selfadjointView<Eigen::Lower>()*HH_cp);
   std::cout<< JJmat.cols()<< " and "<< JJmat.rows()<<std::endl;
    	   bin_write(dirname+"/JJmat.bin", JJmat);
   	     bin_write(dirname+"/E.bin", eigenVals);
         while(i*dt<tot)
        {
	  std::complex<double> val=(newIn_L.adjoint()*(JJcp.selfadjointView<Eigen::Lower>()*newIn))(0);
  std::vector<std::complex<double>> JJ_vec;
   std::vector<double> t_vec;
  JJ_vec.push_back(val);
   t_vec.push_back(i*dt);
   size_t vecSize=t_vec.size();
       ToFile(JJ_vec, dirname+"/JJ.dat",vecSize);      
       ToFile(t_vec, dirname+"/time.dat",vecSize);      
  	 TimeEv::timeev_exact(newIn, cEVec, evExp);
  	 	 TimeEv::timeev_exact(newIn_L, cEVec, evExp_MIN);
    	 i++;
 	 std::cout<< i*dt<<"  at "<<val << " norm "<<newIn_L.norm()<<std::endl;
        } 	   


  
  

  return 0;
}
 
