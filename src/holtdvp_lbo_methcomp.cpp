#include "itensor/all.h"
#include"files.hpp"
#include"holstein_withspin.hpp"
#include"makehamiltonians.hpp"
#include <iostream>
#include <iomanip>
#include"tdvp_lbo.hpp"
#include <boost/program_options.hpp>
#include <chrono>

using namespace std;
using namespace itensor;

int main(int argc, char *argv[])
{
  using itensor::MPO;
  using itensor::MPS;
  using itensor::AutoMPO;

  double cutoff{};
  int M{};
  int Md{};
  int L{};

  double t0{};
  double omega{};
  double omegap{};
  double gamma{};
  double dt{};
  double tot{};
  int maxSweeps{};
  std::string DIR{};
  std::string scutoff{};
  std::string sM{};
  std::string sL{};
  std::string slbocutoff{};
  double lbocutoff{};
  std::string st0{};
  std::string sMS{};
  std::string somega{};
  std::string somegap{};
  std::string sgamma{};
  std::string sdt{};
  std::string stot{};

  std::string filename="holtdvplbo";
  try
  {
    boost::program_options::options_description desc{"Options"};
    desc.add_options()
      ("help,h", "Help screen")
      ("L", boost::program_options::value(&L)->default_value(4), "L")
      ("M", boost::program_options::value(&M)->default_value(4), "M")
      ("t0", boost::program_options::value(&t0)->default_value(1.0), "t0")
      ("omg", boost::program_options::value(&omega)->default_value(1.0), "omg")
      ("omgp", boost::program_options::value(&omegap)->default_value(1.0), "omgp")
      ("gam", boost::program_options::value(&gamma)->default_value(1.0), "gam")
      ("Md", boost::program_options::value(&Md)->default_value(100), "gam")
      ("dt", boost::program_options::value(&dt)->default_value(0.01), "dt")
      ("tot", boost::program_options::value(&tot)->default_value(1.0), "tot")
      ("lbocut", boost::program_options::value(&lbocutoff)->default_value(1E-9), "lbocut")
      ("d", boost::program_options::value(&DIR)->default_value("NODIR"), "d")
      ("MS", boost::program_options::value(&maxSweeps)->default_value(40), "MS")
      ("cut", boost::program_options::value(&cutoff)->default_value(1E-15), "cut");
    
  


    boost::program_options::variables_map vm;
    boost::program_options::store(parse_command_line(argc, argv, desc), vm);
    boost::program_options::notify(vm);
    
    if (vm.count("help"))
      {std::cout << desc << '\n'; return 0;}
    else{
      if (vm.count("L"))
      {      std::cout << "L: " << vm["L"].as<int>() << '\n';
      	sL="L"+std::to_string(vm["L"].as<int>());
	filename+=sL;
      }
   
      	     if (vm.count("M"))
      {      std::cout << "M: " << vm["M"].as<int>() << '\n';
      	sM="M"+std::to_string(vm["M"].as<int>());
      	filename+=sM;
      }
      	 if (vm.count("t0"))
      {      std::cout << "t0: " << vm["t0"].as<double>() << '\n';
      	st0="t0"+std::to_string(vm["t0"].as<double>()).substr(0, 3);
      	filename+=st0;
      }
      
      		 if (vm.count("gam"))
      {      std::cout << "gamma: " << vm["gam"].as<double>() << '\n';
      	sgamma="gam"+std::to_string(vm["gam"].as<double>()).substr(0, 3);
      	filename+=sgamma;
      }
      	 	 if (vm.count("omg"))
      {      std::cout << "omega: " << vm["omg"].as<double>() << '\n';
      	somega="omega"+std::to_string(vm["omg"].as<double>()).substr(0, 3);
      		filename+=somega;
      }
      	 	 if (vm.count("omgp"))
      {      std::cout << "omegap: " << vm["omgp"].as<double>() << '\n';
      	somegap="omegap"+std::to_string(vm["omgp"].as<double>()).substr(0, 3);
      		filename+=somegap;
      }
		 if (vm.count("lbocut"))
		   {
		std::cout << "lbocutoff: " << vm["lbocut"].as<double>() << '\n';
	 std::stringstream ss;
	 ss<<vm["lbocut"].as<double>();
	 slbocutoff="lbocut"+ss.str();
      	filename+=slbocutoff;
      }
      		 if (vm.count("tot"))
      {      std::cout << "tot: " << vm["tot"].as<double>() << '\n';
      	stot="tot"+std::to_string(vm["tot"].as<double>()).substr(0, 3);
      		filename+=stot;
      }
      		 if (vm.count("dt"))
      {      std::cout << "dt: " << vm["dt"].as<double>() << '\n';
      	sdt="dt"+std::to_string(vm["dt"].as<double>()).substr(0, 6);
      		filename+=sdt;
      }
      		 if (vm.count("cut"))
      {      std::cout << "cutoff: " << vm["cut"].as<double>() << '\n';
	 std::stringstream ss;
	 ss<<vm["cut"].as<double>();
	 scutoff="cut"+ss.str();
      	filename+=scutoff;
      }     if (vm.count("Md"))
      {      std::cout << "Md: " << vm["Md"].as<int>() << '\n';
      	
      	filename+="M"+std::to_string(vm["Md"].as<int>());;
      }
	     if (vm.count("MS"))
      {      std::cout << "Max SWEEPS: " << vm["MS"].as<int>() << '\n';
      	sMS="MS"+std::to_string(vm["MS"].as<int>());
      	filename+=sMS;
      }
      }
    }
  catch (const boost::program_options::error &ex)
  {
    std::cerr << ex.what() << '\n';
    return 0;
  }
  using std::chrono::high_resolution_clock;
  using std::chrono::duration_cast;
  using std::chrono::duration;
  using std::chrono::milliseconds;




     auto sites = Holstein_withspin_one(L,{"ConserveNf=",true,
                             "ConserveNb=",false,
				"MaxOcc",M});
     std::cout<< "L "<< L << " M "<< M << std::endl;
     auto am=makeHolstHam_disp_withspin(sites, t0, gamma, omega, omegap);

 auto H = toMPO(am);
 auto H2 = toMPO(am);
 auto H3 = toMPO(am);
     std::vector<double> sw_noise;
     std::vector<double> sw_cutoff;
     std::vector<int> sw_maxdim;
     std::vector<int> sw_niter;



     for(int i=0; i<maxSweeps/10; i++)
       {
	 sw_noise.push_back(1E-2);
	 sw_cutoff.push_back(1E-5);
	 sw_maxdim.push_back(10);
	 sw_niter.push_back(4);

}

     for(int i=0; i<maxSweeps/10; i++)
       {
	 sw_noise.push_back(1E-5);
	 sw_cutoff.push_back(1E-3);
	 sw_maxdim.push_back(20);
sw_niter.push_back(3);
}

    for(int i=0; i<maxSweeps/5; i++)
       {
	 sw_noise.push_back(1E-8);
	 sw_cutoff.push_back(1E-4);
	 sw_maxdim.push_back(30);
sw_niter.push_back(2);
}
    for(int i=0; i<maxSweeps/5; i++)
       {
	 sw_noise.push_back(1E-10);
	 sw_cutoff.push_back(1E-6);
	 sw_maxdim.push_back(50);
sw_niter.push_back(2);
}
    for(int i=0; i<maxSweeps/5; i++)
       {
	 sw_noise.push_back(0);
	 sw_cutoff.push_back(1E-8);
	 sw_maxdim.push_back(100);
sw_niter.push_back(2);
}
    for(int i=0; i<maxSweeps/5; i++)
       {
	 sw_noise.push_back(0);
	 sw_cutoff.push_back(1E-9);
	 sw_maxdim.push_back(200);
sw_niter.push_back(2);
}

       auto sweeps_gs = Sweeps(maxSweeps);
       for(int i=0; i<int(sw_noise.size()); i++)
	 {
	   sweeps_gs.setcutoff(i, sw_cutoff[i]);
	   sweeps_gs.setnoise(i, sw_noise[i]);
	   sweeps_gs.setmaxdim(i, sw_maxdim[i]);
}

    auto state = InitState(sites);
    state.setAll("Emp");

         state.set(int((L+1)/2)+1,"Up");
       state.set(int((L+1)/2),"Dn");

   
    auto psi_x = randomMPS(state);
    auto obs=itensor::DMRGObserver(psi_x, {"EnergyErrgoal",1e-12});
    auto args2 = itensor::Args("Cutoff=",0,"MinDim=", 10,"MaxDim=",Md,"CutoffLBO=",0, "MinDimLBO=",0,"Quiet", true);
	  auto [energy,psi0] = itensor::dmrg(H,psi_x,sweeps_gs,obs,args2);
    std::cout<< "out "<< std::endl;
 psi0.position(1);
     psi0.normalize();
     auto E0=itensor::innerC(psi0, H,psi0)/itensor::innerC(psi0, psi0);
 std::cout<< "energy "<< E0<<std::endl;

auto args_mpo = itensor::Args("Cutoff=",cutoff,"MaxDim=",Md, "Normalize", false);



  auto psiOne=applyMPO(H,psi0, args_mpo);

 
  auto E0Sqrd=itensor::innerC(psiOne,psiOne);
 std::cout<< " djei "<< E0Sqrd<<std::endl;
 	  auto VAL=E0*E0;
 std::cout<< " djei 2 2 "<< VAL<<std::endl;
 auto VARIANCE =  std::abs((E0Sqrd-VAL )/E0Sqrd);
 		    std::cout<< "THE REL GS VAR WAS "<< VARIANCE<<std::endl;
 		    std::cout<< "THE GS VAR WAS "<< std::abs(E0Sqrd-VAL )<<std::endl;


		    auto args = Args("Cutoff=",cutoff,"MaxDim=",Md,"CutoffLBO=",lbocutoff,"MaxDimLBO",300, "DoNormalize",true,"MinDimLBO=",0);


 auto curr=makeCurr_withspin(sites, t0);
  auto Curr=toMPO(curr);
  auto psi=applyMPO(Curr,psi0, args_mpo);
     psi.noPrime();
 psi.position(1);

     psi.normalize();
     auto amponph =AutoMPO(sites);
     for(int i=1; i<=L; i++)
       {

	    amponph += 1,"Nph",i;
}

auto O=MPO(amponph);

 
 auto psi2=psi;
auto psi3=psi;



 std::vector<double> obsex;
  std::vector<double> obsgat;
   std::vector<double> entropy;
 std::vector<double> obstebd;
  std::vector<double> time;
  // Print(psi);
   int i=0;
         std::chrono::duration<double> elapsed_seconds;
 std::chrono::time_point<std::chrono::system_clock> start, end;
;

    auto sweeps = Sweeps(1);
    sweeps.maxdim() = Md;
    sweeps.cutoff() = cutoff;
    sweeps.niter() = 10;
    println(sweeps);
    TDVP_lbo<MPS> tdvp_1(psi,H, -dt*Cplx_i,sweeps, args);
    TDVP_lbo<MPS> tdvp_2(psi2,H2, -dt*Cplx_i,sweeps, args);
TDVP_lbo<MPS> tdvp_3(psi3,H3, -dt*Cplx_i,sweeps, args);

    double sum1tot=0;
double sum2tot=0;
double sum3tot=0;

      while(dt*i<tot)
    {
    	 i+=1;


	 
	 auto t1 = high_resolution_clock::now();
	 tdvp_1.step();
	auto t2 = high_resolution_clock::now();
	auto timediff_1 =duration<double,std::milli>(t2 - t1);
	 auto t1_2 = high_resolution_clock::now();
	  	tdvp_2.step_impl2();
	auto t2_2 = high_resolution_clock::now();
	auto timediff_2 =duration<double,std::milli>(t2_2 - t1_2);

auto t1_3 = high_resolution_clock::now();
	  	tdvp_3.step_impl3();
	auto t2_3 = high_resolution_clock::now();
	auto timediff_3 =duration<double,std::milli>(t2_3 - t1_3);

	std::cout<< "time diff first " << timediff_1.count()<< " time diff sec " << timediff_2.count()<<" time diff third " << timediff_3.count()<<std::endl;;
	std::cout<<std::setprecision(12)<<i*dt <<" diff obs  "<<real(innerC(psi, O, psi))-real(innerC(psi2, O, psi2))<< " one minus overlap two "<<std::abs(1.-real(innerC(psi, psi2)))<< " one minus overlap three "<<std::abs(1.-real(innerC(psi, psi3)))<<" max firs "<<maxLinkDim(psi)<< " max sec "<<maxLinkDim(psi2)<<" max third "<<maxLinkDim(psi3)<<std::endl;
		sum1tot+=timediff_1.count();
sum2tot+=timediff_2.count();
	std::cout<< "new sum first " << sum1tot<<" new sum  sec " << sum2tot<<std::endl;

     
  	//    auto avl2 = innerC(psi, Nph(sites), psi);
	std::vector<double> time({i*dt});
	std::vector<double> time_taken1({timediff_1.count()});
	std::vector<double> time_taken2({timediff_2.count()});
	std::vector<double> time_taken3({timediff_3.count()});
	std::vector<double> overlap_2({real(innerC(psi, psi2))});
	std::vector<double> overlap_3({real(innerC(psi, psi3))});
	std::vector<double> O_1({real(innerC(psi, O, psi))});
	std::vector<double> O_2({real(innerC(psi2, O, psi2))});
	std::vector<double> O_3({real(innerC(psi3, O, psi3))});
	std::vector<double> maxlbo_1svd({tdvp_1.get_max_lbo_svd()});
 std::vector<double> maxlbo_2svd({tdvp_2.get_max_lbo_svd()});
 std::vector<double> maxlbo_2solv({tdvp_2.get_max_lbo_solv()});
 std::vector<double> meanlbo_2solv({tdvp_2.get_mean_lbo_solv()});
 std::vector<double> maxlbo_3svd({tdvp_3.get_max_lbo_svd()});
 std::vector<double> maxlbo_3solv({tdvp_3.get_max_lbo_solv()});
 std::vector<double> meanlbo_3solv({tdvp_3.get_mean_lbo_solv()});
 size_t vecSize=meanlbo_2solv.size();
  itensor::ToFile(time, DIR+"/time.dat",vecSize);
  itensor::ToFile(time_taken1,DIR+ "/time_taken_1.dat",vecSize);
  itensor::ToFile(time_taken2,DIR+ "/time_taken_2.dat",vecSize);
  itensor::ToFile(time_taken3,DIR+ "/time_taken_3.dat",vecSize);
   itensor::ToFile(overlap_2,DIR+ "/overlap_2.dat",vecSize);
  itensor::ToFile(overlap_3,DIR+ "/overlap_3.dat",vecSize);
  itensor::ToFile(O_1, DIR+"/Nph_1.dat",vecSize);
  itensor::ToFile(O_2,DIR+ "/Nph_2.dat",vecSize);
  itensor::ToFile(O_3,DIR+ "/Nph_3.dat",vecSize);
  itensor::ToFile(maxlbo_1svd,DIR+ "/maxlbosvd_1.dat",vecSize);
  itensor::ToFile(maxlbo_2svd,DIR+ "/maxlbosvd_2.dat",vecSize);
  itensor::ToFile(maxlbo_2solv, DIR+"/maxlbosolv_2.dat",vecSize);
  itensor::ToFile(meanlbo_2solv, DIR+"/meanlbosolv_2.dat",vecSize);

  itensor::ToFile(maxlbo_3svd,DIR+ "/maxlbosvd_3.dat",vecSize);
  itensor::ToFile(maxlbo_3solv, DIR+"/maxlbosolv_3.dat",vecSize);
  itensor::ToFile(meanlbo_3solv, DIR+"/meanlbosolv_3.dat",vecSize);
 


    }

  return 0;
}
