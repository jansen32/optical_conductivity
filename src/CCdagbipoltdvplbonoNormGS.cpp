

#include "itensor/all.h"
#include"files.hpp"
#include"holstein.hpp"
#include"holstein_withspin.hpp"
#include <iostream>
#include <iomanip>
#include<utility>
#include <boost/program_options.hpp>
#include"tdvp_lbo.hpp"
#include <boost/filesystem.hpp>
#include"write_data.h"
using namespace std;
using namespace itensor;

int main(int argc, char *argv[])
{
  
  double cutoff{};
  double lbocutoff{};
  int M{};
  int Md{};
  int lboMd{};
  int L{};
  int site_1{};
  int site_2{};
     std::string somegap{};
std::string mpsName{};
 std::string siteSetName{};
  double t0{};
  double omega{};
  double gamma{};
  double dt{};
  double omegap{};
  double tot{};
  std::string DIR{};
  std::string stot{};
  std::string obsname;


  std::string filename="";
  try
  {
    boost::program_options::options_description desc{"Options"};
    desc.add_options()
      ("help,h", "Help screen")
      ("mpsN", boost::program_options::value(&mpsName)->default_value("noName"), "mpsN")
      ("siteN", boost::program_options::value(&siteSetName)->default_value("noName"), "siteSetName")
      ("L", boost::program_options::value(&L)->default_value(4), "L")
      ("M", boost::program_options::value(&M)->default_value(4), "M")
      ("i", boost::program_options::value(&site_1)->default_value(1), "i")
      ("j", boost::program_options::value(&site_2)->default_value(2), "j")
      ("Md", boost::program_options::value(&Md)->default_value(200), "Md")
      ("lboMd", boost::program_options::value(&lboMd)->default_value(100), "lboMd")
      ("t0", boost::program_options::value(&t0)->default_value(1.0), "t0")
      ("omg", boost::program_options::value(&omega)->default_value(1.0), "omg")
      ("omgp", boost::program_options::value(&omegap)->default_value(0), "omgp")
      ("gam", boost::program_options::value(&gamma)->default_value(1.0), "gam")
      ("dt", boost::program_options::value(&dt)->default_value(0.01), "dt")
      ("d", boost::program_options::value(&DIR)->default_value("NO Name"), "d")
      ("tot", boost::program_options::value(&tot)->default_value(1.0), "tot")
      ("lbocut", boost::program_options::value(&lbocutoff)->default_value(1E-15), "lbocut")
      ("cut", boost::program_options::value(&cutoff)->default_value(1E-15), "cut");

    boost::program_options::variables_map vm;
    boost::program_options::store(parse_command_line(argc, argv, desc), vm);
    boost::program_options::notify(vm);
    obsname="Cdagi"+std::to_string(vm["i"].as<int>())+"Cj"+std::to_string(vm["j"].as<int>());    
    if (vm.count("help"))
      {std::cout << desc << '\n'; return 0;}
    else{
      if (vm.count("L"))
      {      std::cout << "L: " << vm["L"].as<int>() << '\n';


      }
   
      	     if (vm.count("M"))
      {      std::cout << "M: " << vm["M"].as<int>() << '\n';


      }
      	     if (vm.count("i"))
      {      std::cout << "i: " << vm["i"].as<int>() << '\n';


      }
      	     if (vm.count("j"))
      {      std::cout << "j: " << vm["j"].as<int>() << '\n';


      }
	           	     if (vm.count("Md"))
      {      std::cout << "Md: " << vm["Md"].as<int>() << '\n';


      }
			           	     if (vm.count("lboMd"))
      {      std::cout << "lboMd: " << vm["lboMd"].as<int>() << '\n';


      }
      	 if (vm.count("t0"))
      {      std::cout << "t0: " << vm["t0"].as<double>() << '\n';


      }
	 if (vm.count("gam"))
      {      std::cout << "gamma: " << gamma << '\n';


      }
      	 	 if (vm.count("omg"))
      {      std::cout << "omega: " << vm["omg"].as<double>() << '\n';
      }
		       	 	 if (vm.count("omgp"))
      {      std::cout << "omegap: " << vm["omgp"].as<double>() << '\n';

      }
      		 if (vm.count("tot"))
      {      std::cout << "tot: " << vm["tot"].as<double>() << '\n';

      	stot="tot"+std::to_string(vm["tot"].as<double>()).substr(0, 6);

      }
      		 if (vm.count("dt"))
      {      std::cout << "dt: " << vm["dt"].as<double>() << '\n';

      }
      		 if (vm.count("cut"))
      {      std::cout << "cutoff: " << vm["cut"].as<double>() << '\n';

      }
		 if (vm.count("lbocut"))
		   {
		std::cout << "lbocutoff: " << vm["lbocut"].as<double>() << '\n';

      }

      }
    }
  catch (const boost::program_options::error &ex)
  {
    std::cerr << ex.what() << '\n';
    return 0;
  }

   auto sites = readFromFile<Holstein_withspin_one>(siteSetName);
    auto psi1 = readFromFile<MPS>(mpsName);
    //print(psi);
   using Gate = BondGate;
    auto gates = vector<Gate>();
  int N=L;
   
 double t_in=0;

  auto argsMPS = itensor::Args("Cutoff=",cutoff,"MaxDimLBO",lboMd,"MinLBO",1,"CutoffLBO",lbocutoff  , "MaxDim=",Md, "MinDim=",10,"Truncate", true,"DoNormalize",false);



   std::chrono::duration<double> elapsed_seconds;
 std::chrono::time_point<std::chrono::system_clock> start, end;

MPS psi2=psi1;
    auto argsObs= itensor::Args("Method=","DensityMatrix","MaxDim=",Md,"Cutoff=",cutoff);

    applyCdagdn(psi1, site_2, sites);
    applyCdagdn(psi2, site_1, sites);

	      
	     

 double svdmax_in2=0;
 double svdmax_in1=0;

	   write_data_FT(psi1, psi2, svdmax_in2, svdmax_in1, t_in, argsObs, DIR,obsname, argsMPS);
    auto sweeps = Sweeps(1);
    sweeps.maxdim() = Md;
    sweeps.cutoff() = cutoff;
    sweeps.niter() = 10;

   
    auto am=makeHolstHam_disp_withspin(sites, t0, gamma, omega, omegap);
    auto H = toMPO(am);

    auto  t_step=-0.5*dt*Cplx_i;
    TDVP_lbo<MPS> tdvp1(psi1,H, t_step, sweeps,argsMPS);
    TDVP_lbo<MPS> tdvp2(psi2,H, -t_step,sweeps,argsMPS);

 



 int i=0;

     	   while(t_in +i*dt<tot)
      {
 	  	tdvp1.step_impl2();
	  	tdvp2.step_impl2();
	   	   i++;
	std::cout<< i << " and "<< t_in +i*dt << "  and "   << tot << std::endl;     
	double svdmax_1=tdvp1.get_max_lbo_svd();
	double svdmax_2=tdvp2.get_max_lbo_svd();
	write_data_FT(psi1, psi2,svdmax_2 ,svdmax_1, t_in +i*dt, argsObs, DIR, obsname, argsMPS);
 		       std::vector<double> solvmeanK_vec={tdvp1.get_max_lbo_solv()};
		       std::vector<double> solvmeanB_vec={tdvp2.get_max_lbo_solv()};

		       std::vector<double> solvmaxK_vec={tdvp1.get_max_lbo_solv()};
		       std::vector<double> solvmaxB_vec={tdvp2.get_max_lbo_solv()};

    auto vecSize=solvmaxK_vec.size();
    			   
    itensor::ToFile(solvmeanK_vec, DIR+"/lbo_solvmeanK.dat", vecSize);
    itensor::ToFile(solvmeanB_vec, DIR+"/lbo_solvmeanB.dat", vecSize);

    itensor::ToFile(solvmaxK_vec, DIR+"/lbo_solvmxK.dat", vecSize);
    itensor::ToFile(solvmaxB_vec, DIR+"/lbo_solvmxB.dat", vecSize);      
}


	   bool apply_op=true;
		       write_state( psi1, psi2, mpsName,  mpsName,  DIR,  stot, apply_op,apply_op);





  return 0;
}
