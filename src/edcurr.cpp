#include"holsteinoperators.hpp"
#include"operators.hpp"
#include"timeev.hpp"
#include"diag.h"
#include "files.hpp"
#include <string>
#include"val.hpp"
#include"src/sparseev.hpp"
#include <boost/program_options.hpp>
#define MOM

using namespace boost::program_options;
using namespace Eigen;
using namespace std;
using namespace Many_Body;
using namespace Holstein_Operators;
    using Mat= Operators::Mat;
int main(int argc, char *argv[])
{
    using HolsteinBasis= TensorProduct<ElectronBasis,PhononBasis>;
  size_t M{};
  size_t L{};
   int k{};
  double t0{};
  double omega{};
  double omega2{};
  double gamma{};
  bool PB{};
  std::string filename{};
  try
  {
    options_description desc{"Options"};
    desc.add_options()
      ("help,h", "Help screen")
      ("L", value(&L)->default_value(4), "L")
      ("M", value(&M)->default_value(1), "M")
      ("t", value(&t0)->default_value(1.), "t0")
      ("k", value(&k)->default_value(1), "k")
      ("gam", value(&gamma)->default_value(1./std::sqrt(2)), "gamma")
      ("omg", value(&omega)->default_value(1.), "omega")
      ("omg2", value(&omega2)->default_value(1.), "omega2")
    ("pb", value(&PB)->default_value(true), "PB");
  


    variables_map vm;
    store(parse_command_line(argc, argv, desc), vm);
    notify(vm);

    if (vm.count("help"))
      {std::cout << desc << '\n'; return 0;}
    else{
      if (vm.count("L"))
      {      std::cout << "L: " << vm["L"].as<size_t>() << '\n';
		filename+="L"+std::to_string(vm["L"].as<size_t>());
	
      }
            if (vm.count("k"))
	      {
		std::cout << "k: " << vm["k"].as<int>()<<'\n';
		filename+="k"+std::to_string(vm["k"].as<int>());
	
      }
     if (vm.count("M,m"))
      {
	std::cout << "M: " << vm["M"].as<size_t>() << '\n';
		filename+="M"+std::to_string(vm["M"].as<size_t>());
      }
      if (vm.count("t"))
      {
	std::cout << "t0: " << vm["t"].as<double>() << '\n';
		filename+="t"+std::to_string(vm["t"].as<double>()).substr(0,6);
      }
       if (vm.count("omg"))
      {
	std::cout << "omega: " << vm["omg"].as<double>() << '\n';
		filename+="omg"+std::to_string(vm["omg"].as<double>()).substr(0,6);
      }
              if (vm.count("omg2"))
      {
	std::cout << "omega2: " << vm["omg2"].as<double>() << '\n';
		filename+="omg2"+std::to_string(vm["omg2"].as<double>()).substr(0,6);
      }
       if (vm.count("gam"))
      {
	std::cout << "gamma: " << vm["gam"].as<double>() << '\n';
		filename+="gamm"+std::to_string(vm["gam"].as<double>()).substr(0,8);
      }
              if (vm.count("pb"))
      {
	std::cout << "PB: " << vm["pb"].as<bool>() << '\n';
      }
    }
  }
  catch (const error &ex)
  {
    std::cerr << ex.what() << '\n';
    return 0;
  }   
  filename+=".bin";
  std::vector<size_t> ee(L, 0);
      ee[L-1]=1;
           ElectronState aa(ee);
	   // std::cout<< aa<<std::endl;
	    std::cout<< "and "<<aa[L-1]<<std::endl;
  ElectronBasis e(aa);

  PhononBasis ph{ L, M};

  HolsteinBasis TP(e, ph);

  Mat H(TP.dim, TP.dim);
  Eigen::MatrixXcd  eigVec;
  Eigen::VectorXd  eig;
  // Eigen::VectorXcd inistate(TP.dim);
    //    func::bin_read(filename, inistate);
  std::cout<< TP.dim<< std::endl;
  HolsteinComplete(H, TP, ph, t0, gamma, omega, k);
  H+=PhdispOperatorRPH( TP, ph, omega2);
    Eigen::MatrixXcd HH=Eigen::MatrixXcd(H);
  Eigen::VectorXd en(TP.dim);

 Many_Body::diagMatzheev(HH, en);
 // std::cout<< en << std::endl;
 std::cout<<std::setprecision(15)<<en<<std::endl; 
 	func::bin_write("E"+filename,  en);
  // Many_Body::diagMatzheev(HH, en);
  //std::complex<double> enc=(inistate.adjoint()*(H*inistate))(0);
  //  std::cout<< "E "<< enc<<std::endl;
  // std::cout<< "E/E_av "<< enc/(omega*L*M*0.5)<<std::endl;
  // double energy=real(enc);

   
    //    double Emin=energy-dE;
  // double Emax=energy+dE;
  // call(H, TP.dim, eigVec, eig, Emin, Emax, maxdim);
   //      std::string newfilename=filename.substr(0, 2)+"mD"+ std::to_string(maxdim)+"dE"+sdE+ "dblHL"+std::to_string(L)+"M"+std::to_string(M)+"t0_"+std::string(argv[3])+"gam"+sgam+"omg"+ somg+"k"+sk+ ".bin";
        

	        {

	// Eigen::MatrixXcd Energy2=eigVec.adjoint()*H*eigVec;
	// Eigen::VectorXcd v=Energy2.diagonal();
	// func::bin_write("exaEn"+newfilename, v);
	// func::bin_write("eigEn"+newfilename, eig);
	// std::cout<< v.mean()<<std::endl;
	
      }
  		      {
			// double check if correct bdag b
			// make sure sum is taken
			//			Mat  EK=PHcurrOperator(TP, ph , 2, omega2*omega2);

			Mat EK=-ecurrOperator(TP, ph , 2, t0*t0,  k);
			EK+=-currcoupOperator(TP, ph ,  gamma*t0, k); //check this one
 EK+=(BosonCOperator(TP, ph , gamma*omega2,  k,L-2 )-BosonDOperator(TP, ph , gamma*omega2,  k, L-2));
EK+=-PHcurrOperator(TP, ph , 1, omega*omega2);
EK+=-PHcurrOperator(TP, ph , 2, omega2*omega2);
Mat EK1=-ecurrOperator(TP, ph , 2, t0*t0,  k);
Mat EK2=-currcoupOperator(TP, ph ,  gamma*t0, k);
 Mat EK3=(BosonCOperator(TP, ph , gamma*omega2,  k, L-2)-BosonDOperator(TP, ph , gamma*omega2,  k,L-2));
 Mat EK4=-PHcurrOperator(TP, ph , 1, omega*omega2);
 Mat EK5=-PHcurrOperator(TP, ph , 2, omega2*omega2);
			//++PHcurrOperator(TP, ph , 1, omega*omega2)+
			//++

  	 Eigen::MatrixXcd EK20=HH.adjoint()*EK*HH;
  	 Eigen::MatrixXcd EK21=HH.adjoint()*EK1*HH;
  	 Eigen::MatrixXcd EK22=HH.adjoint()*EK2*HH;
	 Eigen::MatrixXcd EK23=HH.adjoint()*EK3*HH;
	 Eigen::MatrixXcd EK24=HH.adjoint()*EK4*HH;
	 Eigen::MatrixXcd EK25=HH.adjoint()*EK5*HH;
	 // auto Tr=EK2.trace();
	 // Tr/=TP.dim;
	 
	 //auto Norm=EK2.lpNorm<2>();
	 //  	 Eigen::VectorXcd v=EK22.diagonal();
  	// func::bin_write("exaEK"+newfilename, v);
	 std::cout<< EK20(0,0)<< "  " <<EK21(0,0) +EK22(0,0)+EK23(0,0)+EK24(0,0)+EK25(0,0)<<std::endl;
	//  std::cout<<"mean "<< v.mean()<<std::endl;
	 // 	 func::bin_write("ecur"+filename,  v);
	 func::bin_write("matecur" + filename, EK20);
	 func::bin_write("0matecur" + filename, EK21);
func::bin_write("1matecur" + filename, EK22);
func::bin_write("2matecur" + filename, EK23);
func::bin_write("3matecur" + filename, EK24);
func::bin_write("4matecur" + filename, EK25);

      }
      // 		        		      {
      // 			// double check if correct bdag b
      // 			// make sure sum is taken
       			Mat  EK=-ecurrOperator(TP, ph , 1, t0*t0,  k);
      // 			//					std::cout<<Eigen::MatrixXcd(EK)<<std::endl;
       	 Eigen::MatrixXcd EK2=HH.adjoint()*EK*HH;
      // 	 // auto Tr=EK2.trace();
      // 	 // Tr/=TP.dim;
	 
      // 	 //auto Norm=EK2.lpNorm<2>();
       	 Eigen::VectorXcd v=EK2.diagonal();
      // 	// func::bin_write("exaEK"+newfilename, v);
      // 	 std::cout<<endl<< v<<std::endl;
      // 	 std::cout<<"mean "<< v.mean()<<std::endl;
	 //    	func::bin_write("cur"+filename,  v);
       	 func::bin_write("matcur" + filename, EK2);
      // }
  // 		      		      {
  // 	Mat MoDF=MDF(TP, ph, k, 0);
	
  // 	Eigen::MatrixXcd MDF2=eigVec.adjoint()*MoDF*eigVec;
  // 	Eigen::VectorXcd v=MDF2.diagonal();
  // 	func::bin_write("exaMDF"+newfilename, v);
  // 		std::cout<< v.mean()<<std::endl;
  //     }
  // 		      		      {
  // 	  Mat J=(2*JumpHer(TP, ph, 1., k));
	
  // 	Eigen::MatrixXcd J2=eigVec.adjoint()*J*eigVec;
  // 	Eigen::VectorXcd v=J2.diagonal();
  // 	func::bin_write("exa2JUMPHer"+newfilename, v);
  // 		std::cout<< v.mean()<<std::endl;
  //     }
  // Mat PHD=NumberOperator(TP, ph, 1.)/L;

	
  // 	Eigen::MatrixXcd PHD2=eigVec.adjoint()*PHD*eigVec;
  // 	Eigen::VectorXcd v=PHD2.diagonal();
  // 	func::bin_write("exaPHD"+newfilename,  v);
  // 		std::cout<< v.mean()<<std::endl;
  return 0;
}
