
#include "itensor/all.h"
#include"files.hpp"
#include"holstein.hpp"
#include"maketimeevop.hpp"
#include"makehamiltonians.hpp"
#include <iostream>
#include <iomanip>
#include<utility>
#include <boost/program_options.hpp>
#include"GFopt.hpp"
#include <boost/filesystem.hpp>
#include"timeevclass.hpp"
#include"lboclass.hpp"
using namespace std;
using namespace itensor;

int main(int argc, char *argv[])
{
  
  double cutoff{};
  double lbocutoff{};
  int M{};
  int j{};
  int Md{};
  int lboMd{};
  int L{};
     std::string somegap{};
std::string mpsName{};
 std::string siteSetName{};
  double t0{};
  double omega{};
  double gamma{};
  double dt{};
  double omegap{};
  double tot{};


  std::string dirname="";
  try
  {
    boost::program_options::options_description desc{"Options"};
    desc.add_options()
      ("help,h", "Help screen")
      ("mpsN", boost::program_options::value(&mpsName)->default_value("noName"), "mpsN")
      ("siteN", boost::program_options::value(&siteSetName)->default_value("noName"), "siteSetName")
      ("L", boost::program_options::value(&L)->default_value(4), "L")
      ("j", boost::program_options::value(&j)->default_value(1), "j")
      ("M", boost::program_options::value(&M)->default_value(4), "M")
      ("Md", boost::program_options::value(&Md)->default_value(200), "Md")
      ("lboMd", boost::program_options::value(&lboMd)->default_value(100), "lboMd")
      ("t0", boost::program_options::value(&t0)->default_value(1.0), "t0")
      ("omg", boost::program_options::value(&omega)->default_value(1.0), "omg")
      ("omgp", boost::program_options::value(&omegap)->default_value(0), "omgp")
      ("gam", boost::program_options::value(&gamma)->default_value(1.0), "gam")
      ("dt", boost::program_options::value(&dt)->default_value(0.01), "dt")
      ("tot", boost::program_options::value(&tot)->default_value(1.0), "tot")
      ("lbocut", boost::program_options::value(&lbocutoff)->default_value(1E-15), "lbocut")
      ("cut", boost::program_options::value(&cutoff)->default_value(1E-15), "cut") 
    ("d", boost::program_options::value(&dirname)->default_value("./"), "d");

    boost::program_options::variables_map vm;
    boost::program_options::store(parse_command_line(argc, argv, desc), vm);
    boost::program_options::notify(vm);
    
    if (vm.count("help"))
      {std::cout << desc << '\n'; return 0;}
    else{
      if (vm.count("L"))
      {      std::cout << "L: " << vm["L"].as<int>() << '\n';

      }
   
      	     if (vm.count("M"))
      {      std::cout << "M: " << vm["M"].as<int>() << '\n';

      }

	           	     if (vm.count("Md"))
      {      std::cout << "Md: " << vm["Md"].as<int>() << '\n';

      }
			           	     if (vm.count("M"))
      {      std::cout << "lboMd: " << vm["lboMd"].as<int>() << '\n';
      }
      	 if (vm.count("t0"))
      {      std::cout << "t0: " << vm["t0"].as<double>() << '\n';
      }
	 if (vm.count("gam"))
      {      std::cout << "gamma: " << gamma << '\n';

      }
      	 	 if (vm.count("omg"))
      {      std::cout << "omega: " << vm["omg"].as<double>() << '\n';

      }
		       	 	 if (vm.count("omgp"))
      {      std::cout << "omegap: " << vm["omgp"].as<double>() << '\n';
      }
      		 if (vm.count("tot"))
      {      std::cout << "tot: " << vm["tot"].as<double>() << '\n';
      }
      		 if (vm.count("dt"))
      {      std::cout << "dt: " << vm["dt"].as<double>() << '\n';

      }
      		 if (vm.count("cut"))
      {      std::cout << "cutoff: " << vm["cut"].as<double>() << '\n';

      }
		 if (vm.count("lbocut"))
		   {
		std::cout << "lbocutoff: " << vm["lbocut"].as<double>() << '\n';

      }

      }
    }
  catch (const boost::program_options::error &ex)
  {
    std::cerr << ex.what() << '\n';
    return 0;
  }

   auto sites = readFromFile<Holstein>(siteSetName);
    auto psi = readFromFile<MPS>(mpsName);
    //print(psi);
   using Gate = BondGate;
    auto gates = vector<Gate>();
  int N=L;
   
   std::vector<int> v(N, M);
   itensor::Args argsState={"ConserveNf=",true,
                              "ConserveNb=",false,
 			   "DiffMaxOcc=",true, "MaxOccVec=", v};


   auto argsMPS = itensor::Args("Cutoff=",cutoff,"MaxDimLBO",lboMd, "CutoffLBO",lbocutoff  , "MaxDim=",Md, "Normalize",false,"SVDMethod", "gesdd");



   std::chrono::duration<double> elapsed_seconds;
 std::chrono::time_point<std::chrono::system_clock> start, end;
   
MPS psi2=psi;

   auto curr= makeCurr(sites, t0);
    auto Curr = toMPO(curr);
    auto am=makeHolstHam_disp(sites, t0, gamma, omega, omegap);
    auto H = toMPO(am);
    auto argsObs= itensor::Args("Method=","DensityMatrix","MaxDim=",Md,"Cutoff=",cutoff);
      std::map<std::string, double> param;
  param.insert({"t0", t0});
  param.insert({"gamma", gamma});
   param.insert({"omega", omega});
   param.insert({"omegap", omegap});
    IHG GM(sites, gates, dt, param);
    //GM.makeGates()
 TimeEvolveLbo< IHG, decltype(psi), false> C1(GM,  psi, N,  argsMPS, argsState);
 TimeEvolveLbo< IHG, decltype(psi), false> C2(GM,  psi2, N,  argsMPS, argsState);   
	      std::cout<< "start energy "<<innerC(psi2,H, psi)<<std::endl;
	      std::cout<< "start obs "<<innerC(psi2,Curr, psi)<<std::endl;
 auto y1 = applyMPO(Curr,psi,argsObs);
 y1.noPrime();
std::cout<< "start obs sqer "<<innerC(psi2,Curr, y1)<<std::endl;
 std::string obsname="JJ";

				       	         GFactMPSGStdmrglbo(C1, psi, C2, psi2, sites, Curr, dt, tot, dirname, M, obsname, argsObs);

				 return 0;
}
