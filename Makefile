LIBRARY_DIR=$(ITENSOR)
include $(LIBRARY_DIR)/this_dir.mk
include $(LIBRARY_DIR)/options.mk
INC+=-I${ITENSOR}
INC+=-I${TENSORFT}/include/
INC+=-I${TENSORTIMEEV}/include/
INC+=-I${TENSOR}/TDVP/
INC+=-I${TENSOR}/parallel_tdvp_lbo
INC+=-I${TENSORTOOLS}/include
INC+=-I$(PWD)/include
INC+=-I${EIGEN}
INC+=-I${EINC}


LIBSLINK+=-L${MANYBODY}/libs
LIBSLINK+=-L${ELIBS}
LIBSPATH=-L$(ITENSOR)/lib
LIBSPATH+=$(LIBSLINK)



LIBS=-litensor -lboost_program_options
LIBSG=-litensor-g -lboost_program_options

#################################
# ED code 
###############################
INC_ED=-I${MANYBODY}/include
INC_ED+=-I${EIGEN}
INC_ED+=-I${EINC}
LIBSLINK_ED=-L${MANYBODY}/libs -L${ELIBS}
LIBS_ED=-leigenmkl32 -lboost_program_options
MKLLINK_ED=-DMKL_Complex16="std::complex<double>" -m64 -I${MKLROOT}/include -L${MKLROOT}/lib/intel64 -Wl,--no-as-needed -lmkl_intel_lp64 -lmkl_gnu_thread -lmkl_core -lgomp -lpthread -lm -ldl

#########################

CCFLAGS+=-I. $(ITENSOR_INCLUDEFLAGS) $(OPTIMIZATIONS) -Wno-unused-variable -std=c++17 -O2 -std=gnu++1z
CCGFLAGS+=-I. $(ITENSOR_INCLUDEFLAGS) $(DEBUGFLAGS) 

LIBFLAGS=-L$(ITENSOR_LIBDIR) $(ITENSOR_LIBFLAGS) -lboost_program_options -lboost_filesystem 
LIBGFLAGS=-L$(ITENSOR_LIBDIR) $(ITENSOR_LIBGFLAGS) -lboost_program_options -lboost_filesystem 
MPICOM=mpic++ -m64 -std=c++17 -fconcepts -fPIC

#LIBFLAGS=-L$(ITENSOR_LIBDIR) $(ITENSOR_LIBFLAGS) -Wl,--start-group /home/jansen32/usr/lib2/lib/libboost_program_options.a /home/jansen32/usr/lib2/lib/libboost_filesystem.a -Wl,--end-group 

CPPFLAGS_EXTRA += -O2 -std=c++17
#MPILINK= -lboost_serialization -lboost_mpi  -I$MPI_INCLUDE -L$MPI_LIB 


DB=-g
CXX=g++
ND=-DNDEBUG

#-fopenmp -DOMPPAR

#
#BINS=


##################################################
holGSdis: src/holGSdis.cpp $(ITENSOR_LIBS)
	$(CCCOM)  $< -o bin/$@ $(CCFLAGS) $(INC) $(LIBFLAGS)

holGSquarterfilldis: src/holGSquarterfilldis.cpp $(ITENSOR_LIBS)
	$(CCCOM)  $< -o bin/$@ $(CCFLAGS) $(INC) $(LIBFLAGS)

holGSbipoldis: src/holGSbipoldis.cpp $(ITENSOR_LIBS)
	$(CCCOM)  $< -o bin/$@ $(CCFLAGS) $(INC) $(LIBFLAGS)

holGSwithspinbipoldis: src/holGSwithspinbipoldis.cpp $(ITENSOR_LIBS)
	$(CCCOM)  $< -o bin/$@ $(CCFLAGS) $(INC) $(LIBFLAGS)
holGSwithspinbipoldis_2: src/holGSwithspinbipoldis_2.cpp $(ITENSOR_LIBS)
	$(CCCOM)  $< -o bin/$@ $(CCFLAGS) $(INC) $(LIBFLAGS)

holFTtdmrglboNoeFast: src/holFTtdmrglboNoeFast.cpp $(ITENSOR_LIBS) 
	$(CCCOM) $< -o bin/$@ $(CCFLAGS) $(INC) $(LIBFLAGS) 

holFTtdmrglboFast: src/holFTtdmrglboFast.cpp $(ITENSOR_LIBS) 
	$(CCCOM) $< -o bin/$@ $(CCFLAGS) $(INC) $(LIBFLAGS) 

holFTtdmrglboFast_otherTs: src/holFTtdmrglboFast_otherTs.cpp $(ITENSOR_LIBS) 
	$(CCCOM) $< -o bin/$@ $(CCFLAGS) $(INC) $(LIBFLAGS) 

holFTtdvp_lbo_ts: src/holFTtdvp_lbo_ts.cpp $(ITENSOR_LIBS) 
	$(CCCOM) $< -o bin/$@ $(CCFLAGS) $(INC) $(LIBFLAGS) 

holFTwithspinbipoltdvp_lbo_ts: src/holFTwithspinbipoltdvp_lbo_ts.cpp $(ITENSOR_LIBS) 
	$(CCCOM) $< -o bin/$@ $(CCFLAGS) $(INC) $(LIBFLAGS) 
holFTwithspinbipoltdvp_lbo_tsminD: src/holFTwithspinbipoltdvp_lbo_tsminD.cpp $(ITENSOR_LIBS) 
	$(CCCOM) $< -o bin/$@ $(CCFLAGS) $(INC) $(LIBFLAGS) 
holFTwithspinbipoltdvp_lbo_ts_T2: src/holFTwithspinbipoltdvp_lbo_ts_T2.cpp $(ITENSOR_LIBS) 
	$(CCCOM) $< -o bin/$@ $(CCFLAGS) $(INC) $(LIBFLAGS) 

holFTwithspinOneeltdvp_lbo_ts: src/holFTwithspinOneeltdvp_lbo_ts.cpp $(ITENSOR_LIBS) 
	$(CCCOM) $< -o bin/$@ $(CCFLAGS) $(INC) $(LIBFLAGS) 

holFTtdvp_lbo_tsT1: src/holFTtdvp_lbo_tsT1.cpp $(ITENSOR_LIBS) 
	$(CCCOM) $< -o bin/$@ $(CCFLAGS) $(INC) $(LIBFLAGS) 
holFTtdvp_lbo_tsminD: src/holFTtdvp_lbo_tsminD.cpp $(ITENSOR_LIBS) 
	$(CCCOM) $< -o bin/$@ $(CCFLAGS) $(INC) $(LIBFLAGS) 

holFTquarterfilledtdvp_lbo_ts: src/holFTquarterfilledtdvp_lbo_ts.cpp $(ITENSOR_LIBS) 
	$(CCCOM) $< -o bin/$@ $(CCFLAGS) $(INC) $(LIBFLAGS) 
holFTtdvp_lbo_ts_otherTs: src/holFTtdvp_lbo_ts_otherTs.cpp $(ITENSOR_LIBS) 
	$(CCCOM) $< -o bin/$@ $(CCFLAGS) $(INC) $(LIBFLAGS) 

holFTtdvp_lbo_tsKRY: src/holFTtdvp_lbo_tsKRY.cpp $(ITENSOR_LIBS) 
	$(CCCOM) $< -o bin/$@ $(CCFLAGS) $(INC) $(LIBFLAGS) 

holFTW2: src/holFTW2.cpp $(ITENSOR_LIBS) 
	$(CCCOM) $< -o bin/$@ $(CCFLAGS) $(INC) $(LIBFLAGS) 

holtdvp_lbo_methcomp: src/holtdvp_lbo_methcomp.cpp $(ITENSOR_LIBS) 
	$(CCCOM) $< -o bin/$@ $(CCFLAGS) $(INC) $(LIBFLAGS) 

CdagCbipoltdvplbonoNormGS: src/CdagCbipoltdvplbonoNormGS.cpp $(ITENSOR_LIBS) 
	$(CCCOM) $< -o bin/$@ $(CCFLAGS) $(INC) $(LIBFLAGS) 

CCdagbipoltdvplbonoNormGS: src/CCdagbipoltdvplbonoNormGS.cpp $(ITENSOR_LIBS) 
	$(CCCOM) $< -o bin/$@ $(CCFLAGS) $(INC) $(LIBFLAGS) 

###############################
## serial programs
# tdvp
JJtdvplbonoNormAux: src/JJtdvplbonoNormAux.cpp $(ITENSOR_LIBS) 
	$(CCCOM) $< -o bin/$@ $(CCFLAGS) $(INC) $(LIBFLAGS) 

JJtdvplbonoNorm: src/JJtdvplbonoNorm.cpp $(ITENSOR_LIBS) 
	$(CCCOM) $< -o bin/$@ $(CCFLAGS) $(INC) $(LIBFLAGS) 
JJtdvplbonoNormGS: src/JJtdvplbonoNormGS.cpp $(ITENSOR_LIBS) 
	$(CCCOM) $< -o bin/$@ $(CCFLAGS) $(INC) $(LIBFLAGS) 

JJFTtdvpwithspinOneSite: src/JJFTtdvpwithspinOneSite.cpp $(ITENSOR_LIBS) 
	$(CCCOM) $< -o bin/$@ $(CCFLAGS) $(INC) $(LIBFLAGS) 

JJFTtdvpwithspinOneSiteMinD: src/JJFTtdvpwithspinOneSiteMinD.cpp $(ITENSOR_LIBS) 
	$(CCCOM) $< -o bin/$@ $(CCFLAGS) $(INC) $(LIBFLAGS) 
JJFTtdvpOneSite: src/JJFTtdvpOneSite.cpp $(ITENSOR_LIBS) 
	$(CCCOM) $< -o bin/$@ $(CCFLAGS) $(INC) $(LIBFLAGS) 

JJFTtdvpOneSiteMinD: src/JJFTtdvpOneSiteMinD.cpp $(ITENSOR_LIBS) 
	$(CCCOM) $< -o bin/$@ $(CCFLAGS) $(INC) $(LIBFLAGS) 



# tdmrg

JJtdmrglbonoNormAux: src/JJtdmrglbonoNormAux.cpp $(ITENSOR_LIBS) 
	$(CCCOM) $< -o bin/$@ $(CCFLAGS) $(INC) $(LIBFLAGS) 

JJtdmrglbonoNormGS: src/JJtdmrglbonoNormGS.cpp $(ITENSOR_LIBS) 
	$(CCCOM) $< -o bin/$@ $(CCFLAGS) $(INC) $(LIBFLAGS) 



JJFTSS_timeCont: src/JJFTSS_timeCont.cpp $(ITENSOR_LIBS)  
	$(CCCOM) $< -o bin/$@ $(CCFLAGS) $(INC) $(LIBFLAGS) 


print_state: src/print_state.cpp $(ITENSOR_LIBS) 
	$(CCCOM) $< -o bin/$@ $(CCFLAGS) $(INC)  $(LIBSPATH) $(LIBFLAGS)  $(ND)

overlap: src/overlap.cpp $(ITENSOR_LIBS) 
	$(CCCOM) $< -o bin/$@ $(CCFLAGS) $(INC)  $(LIBSPATH) $(LIBFLAGS)  $(ND)

overlap_spin: src/overlap_spin.cpp $(ITENSOR_LIBS) 
	$(CCCOM) $< -o bin/$@ $(CCFLAGS) $(INC)  $(LIBSPATH) $(LIBFLAGS)  $(ND)

##########################
# parallell programs
JJpara: src/JJpara.cpp $(ITENSOR_LIBS) 
	$(MPICOM) -std=c++17 $<  -o bin/$@  $(CCFLAGS) $(INC)  $(LIBSPATH) $(LIBFLAGS)  $(ND)

holFTparaholtwospin_state: src/holFTparaholtwospin_state.cpp $(ITENSOR_LIBS) 
	$(MPICOM) -std=c++17 $<  -o bin/$@  $(CCFLAGS) $(INC)  $(LIBSPATH) $(LIBFLAGS)  $(ND)

JJpara_timeCont: src/JJpara_timeCont.cpp $(ITENSOR_LIBS)  
	$(MPICOM) -std=c++17 $<  -o bin/$@  $(CCFLAGS) $(INC)  $(LIBSPATH) $(LIBFLAGS)  $(ND)

JJpara_timeContBT: src/JJpara_timeContBT.cpp $(ITENSOR_LIBS)  
	$(MPICOM) -std=c++17 $<  -o bin/$@  $(CCFLAGS) $(INC)  $(LIBSPATH) $(LIBFLAGS)  $(ND)
JJpara_timeContBTsecimpl: src/JJpara_timeContBTsecimpl.cpp $(ITENSOR_LIBS)  
	$(MPICOM) -std=c++17 $<  -o bin/$@  $(CCFLAGS) $(INC)  $(LIBSPATH) $(LIBFLAGS)  $(ND)

JJpara_timeContBTsecimplEff: src/JJpara_timeContBTsecimplEff.cpp $(ITENSOR_LIBS)  
	$(MPICOM) -std=c++17 $<  -o bin/$@  $(CCFLAGS) $(INC)  $(LIBSPATH) $(LIBFLAGS)  $(ND)


JJpara_timeContsecimplEff: src/JJpara_timeContsecimplEff.cpp $(ITENSOR_LIBS)  
	$(MPICOM) -std=c++17 $<  -o bin/$@  $(CCFLAGS) $(INC)  $(LIBSPATH) $(LIBFLAGS)  $(ND)

JJpara_timeContEff: src/JJpara_timeContEff.cpp $(ITENSOR_LIBS)  
	$(MPICOM) -std=c++17 $<  -o bin/$@  $(CCFLAGS) $(INC)  $(LIBSPATH) $(LIBFLAGS)  $(ND)

JJparawithspin_timeContBTsecimpl: src/JJparawithspin_timeContBTsecimpl.cpp $(ITENSOR_LIBS)  
	$(MPICOM) -std=c++17 $<  -o bin/$@  $(CCFLAGS) $(INC)  $(LIBSPATH) $(LIBFLAGS)  $(ND)

JJparawithspin_timeContBTsecimplEff: src/JJparawithspin_timeContBTsecimplEff.cpp $(ITENSOR_LIBS)  
	$(MPICOM) -std=c++17 $<  -o bin/$@  $(CCFLAGS) $(INC)  $(LIBSPATH) $(LIBFLAGS)  $(ND)

JJparawithspin_timeContsecimplEff: src/JJparawithspin_timeContsecimplEff.cpp $(ITENSOR_LIBS)  
	$(MPICOM) -std=c++17 $<  -o bin/$@  $(CCFLAGS) $(INC)  $(LIBSPATH) $(LIBFLAGS)  $(ND)

JJFTpara_timeCont: src/JJFTpara_timeCont.cpp $(ITENSOR_LIBS) 
	$(MPICOM) -std=c++17 $<  -o bin/$@  $(CCFLAGS) $(INC)  $(LIBSPATH) $(LIBFLAGS)  $(ND)
JJFTpara_timeContBT: src/JJFTpara_timeContBT.cpp $(ITENSOR_LIBS) 
	$(MPICOM) -std=c++17 $<  -o bin/$@  $(CCFLAGS) $(INC)  $(LIBSPATH) $(LIBFLAGS)  $(ND)

JJFTpara_timeContBTsecimpl: src/JJFTpara_timeContBTsecimpl.cpp $(ITENSOR_LIBS) 
	$(MPICOM) -std=c++17 $<  -o bin/$@  $(CCFLAGS) $(INC)  $(LIBSPATH) $(LIBFLAGS)  $(ND)
JJFTpara_timeContsecimpl: src/JJFTpara_timeContsecimpl.cpp $(ITENSOR_LIBS) 
	$(MPICOM) -std=c++17 $<  -o bin/$@  $(CCFLAGS) $(INC)  $(LIBSPATH) $(LIBFLAGS)  $(ND)

JJFTpara_Testing: src/JJFTpara_Testing.cpp $(ITENSOR_LIBS) 
	$(MPICOM) -std=c++17 $<  -o bin/$@  $(CCFLAGS) $(INC)  $(LIBSPATH) $(LIBFLAGS)  $(ND)
JJFTparawithspin_timeContBTsecimpl: src/JJFTparawithspin_timeContBTsecimpl.cpp $(ITENSOR_LIBS) 
	$(MPICOM) -std=c++17 $<  -o bin/$@  $(CCFLAGS) $(INC)  $(LIBSPATH) $(LIBFLAGS)  $(ND)
JJFTparawithspin_timeContsecimpl: src/JJFTparawithspin_timeContsecimpl.cpp $(ITENSOR_LIBS) 
	$(MPICOM) -std=c++17 $<  -o bin/$@  $(CCFLAGS) $(INC)  $(LIBSPATH) $(LIBFLAGS)  $(ND)

JJFTpara: src/JJFTpara.cpp $(ITENSOR_LIBS) 
	$(MPICOM) -std=c++17 $<  -o bin/$@  $(CCFLAGS) $(INC)  $(LIBSPATH) $(LIBFLAGS)  $(ND)
###############
# test
FT_test: src/FT_test.cpp $(ITENSOR_LIBS) 
	$(CCCOM) $< -o bin/$@ $(CCFLAGS) $(INC) $(LIBFLAGS) 
FTwithspin_test: src/FTwithspin_test.cpp $(ITENSOR_LIBS) 
	$(CCCOM) $< -o bin/$@ $(CCFLAGS) $(INC) $(LIBFLAGS) 
GS_test: src/GS_test.cpp $(ITENSOR_LIBS) 
	$(CCCOM) $< -o bin/$@ $(CCFLAGS) $(INC) $(LIBFLAGS) 
GS_eval: src/GS_eval.cpp $(ITENSOR_LIBS) 
	$(CCCOM) $< -o bin/$@ $(CCFLAGS) $(INC) $(LIBFLAGS) 
GSwithspinbipol_test: src/GSwithspinbipol_test.cpp $(ITENSOR_LIBS) 
	$(CCCOM) $< -o bin/$@ $(CCFLAGS) $(INC) $(LIBFLAGS) 


########################################
# ED
holstFTexact: src/holstFTexact.cpp
	$(CXX) $(FLAGS) $(INC_ED) $< -o bin/$@  $(MKLLINK_ED) $(LIBS_ED) $(LIBS_ED) $(LIBSLINK_ED) $(ND) $(OP)

JJHolExGS_timeev: src/JJHolExGS_timeev.cpp
	$(CXX) $(FLAGS) $(INC_ED) $< -o bin/$@  $(MKLLINK_ED) $(LIBS_ED) $(LIBS_ED) $(LIBSLINK_ED) $(ND) $(OP)

JJHolExFT_timeev: src/JJHolExFT_timeev.cpp
	$(CXX) $(FLAGS) $(INC_ED) $< -o bin/$@  $(MKLLINK_ED) $(LIBS_ED) $(LIBS_ED) $(LIBSLINK_ED) $(ND) $(OP)
HolExSPEC: src/HolExSPEC.cpp
	$(CXX) $(FLAGS) $(INC_ED) $< -o bin/$@  $(MKLLINK_ED) $(LIBS_ED) $(LIBS_ED) $(LIBSLINK_ED) $(ND) $(OP)
clean:
	rm bin/*

